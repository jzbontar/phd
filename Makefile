thesis.pdf: *.tex thesis.bib
	xelatex thesis

rebuild:
	make clean
	xelatex thesis
	bibtex thesis || true
	xelatex thesis
	xelatex thesis
	xelatex thesis

cover:
	xelatex cover.tex
	xelatex cover.tex

clean:
	rm -rf *.{aux,log,pdf,bbl,blg,brf,dvi,out,fls,thm,toc}
