\section{Two-View Geometry}

This section reviews the geometry of two perspective views and describes the
process of image formation, epipolar geometry and derives the fundamental matrix
for a parallel camera stereo rig. We follow the presentation given in the
textbook \emph{Multiple View Geometry in Computer Vision} by
\citet{hartley2003multiple}. 


\subsection{Camera Models}

% This section describes the process of image formation, that is, the
% formation of a two-dimensional representation of a three dimensional world. 

The process of image formation is modelled by a central projection. We use bold
lowercase letters ($\mathbf{x}, \mathbf{y}, \mathbf{z}$) to denote 2D column
vectors. Bold uppercase letters ($\mathbf{X}, \mathbf{Y}, \mathbf{Z}$) denote
3D column vectors.  When convenient, we write the elements of the vector
explicitly, such as $(x, y, z)$, in which case, the vector represents a row
vector; that is $\mathbf{X} = (x, y, z)^T$, where $(x, y, z)^T$ is the
transpose of $(x, y, z)$. 

\paragraph{Homogeneous Representation of Lines}

A line in the plane is defined as the set of points $(x, y)$ for which the
following equation holds: $ax + by + c = 0$, where $a$, $b$, and $c$ are real
numbers. The same line can be represented as a 3D column vector $(a, b, c)^T$,
but the correspondence between lines and vectors is not one-to-one. Consider,
for example, vectors $k(a, b, c)^T = (ka, kb, kc)^T$ and $(a, b, c)^T$. The
vectors represent the same line because $ax + by + c = 0$ if and only if $(ka)x
+ (kb)y + kc = 0$ for $k \neq 0$ and, in that sense, the vectors $k(a, b, c)^T$
and $(a, b, c)^T$ are equivalent. Vectors with this equivalence relation are
called {\em homogeneous} vectors.

\paragraph{Homogeneous Representation of Points}

A point in 2D Euclidean space is represented as an ordered pair of real
numbers: $\mathbf{x} = (x, y)^T$. Let $\mathbf{l} = (a, b, c)^T$ represent a
line in the plane. The point $\mathbf{x}$ lies on line $\mathbf{l}$ if and only
if $ax + by + c = 0$. We can write this condition more succinctly using a
single dot product $(x, y, 1) \mathbf{l} = 0$, where the point $(x, y)^T$ is
written as a 3-vector $(x, y, 1)^T$ by appending a final coordinate $1$. The
new representation has an interesting property, namely that the point $(x, y,
1)^T$ lies on line $\mathbf{l}$ if and only if the point $(kx, ky, k)^T = k(x,
y, 1)^T$ lies on line $\mathbf{l}$. It is, therefore, natural to associate $(x,
y, 1)^T$ and $(kx, ky, k)^T$ with the same point $(x, y)$ in $\mathbb{R}^2$.
Similar to the case with lines, points in 2D will often be represented as
homogeneous 3-vector. A point $\mathbf{x} = (x_1, x_2, x_3)^T$ in homogeneous
coordinates lies on a line $\mathbf{l} = (a, b, c)^T$ if and only if the
following holds:
%
\begin{equation} \mathbf{x}^T\mathbf{l} = 0. \label{eqn:point_line}
\end{equation}

To summarize, a point in $\mathbb{R}^2$
can be represented either as an {\em inhomogeneous} 2-vector $(x, y)$ or a {\em
homogeneous} 3-vector $(kx, ky, k)$, for any non-zero value of $k$.

\paragraph{Line joining points}
Let $\mathbf{x}$ and $\mathbf{y}$ denote two points in homogeneous coordinates.
The line $\mathbf{l}$ that passes through points $\mathbf{x}$ and
$\mathbf{y}$ is defined as the cross-product 
%
\begin{equation}
\mathbf{l} = \mathbf{x} \times \mathbf{y}.
\label{eqn:line_passing}
\end{equation}
%
To see this, recall some properties of the scalar triple
product $\mathbf{a}^T (\mathbf{b} \times \mathbf{c})$, namely $\mathbf{a}^T
(\mathbf{a} \times \mathbf{b}) = 0$ and $\mathbf{b}^T (\mathbf{a} \times
\mathbf{b}) = 0$. Line $\mathbf{l}$ passes through point $\mathbf{x}$ if and
only if $\mathbf{x}^T \mathbf{l} = 0$, which holds because $\mathbf{x}^T
\mathbf{l} = \mathbf{x}^T (\mathbf{x} \times \mathbf{y}) = 0$. We can prove
that line $\mathbf{l}$ passes through point $\mathbf{y}$ using a similar
argument.

\paragraph{Cross product as a matrix-vector multiplication}

Let both $\mathbf{x} = (x_1, x_2, x_3)^T$ and $\mathbf{y}$ be 3-vectors. We
wish to represent the cross-product $\mathbf{x} \times \mathbf{y}$ as a
matrix-vector multiplication.  We can achieve this by defining an operator that maps a
3-vector into a $3 \times 3$ matrix:
%
\begin{equation}
[\mathbf{x}]_\times = \begin{bmatrix} 0 & -x_3 & x_2 \\ x_3 & 0 & -x_1 \\ -x_2 & x_1 & 0 \end{bmatrix},
\end{equation}
%
and writing the cross-product as 
%
\begin{equation}
\mathbf{x} \times \mathbf{y} = [\mathbf{x}]_\times \mathbf{y}.
\label{eqn:cp_mv}
\end{equation}

% \paragraph{Homography}
% Let $\mathtt{H}$ be a non-singular $3 \times 3$ matrix. A \emph{homography}, $h$,
% is a linear mapping from homogeneous 3-vectors to homogeneous 3-vectors defined
% as $h(\mathbf{x}) = \mathtt{H}\mathbf{x}$. It holds that three points
% $\mathbf{x}_1$, $\mathbf{x}_2$, and $\mathbf{x}_3$ lie on the same line if and
% only if $h(\mathbf{x}_1)$, $h(\mathbf{x}_2)$, and $h(\mathbf{x}_3)$ do.

\paragraph{Central Projection}
Let $\mathtt{P}$ be a $3 \times 4$ matrix. A \emph{central projection}, $p$, is
a linear mapping from homogeneous 4-vectors to homogeneous 3-vectors defined as
$p(\mathbf{X}) = \mathtt{P}\mathbf{X}$. The matrix $\mathtt{P}$ is known as the
\emph{projection matrix}.

\subsubsection{The Basic Pinhole Camera}

\begin{figure}[tb]
\includegraphics{img/pinhole_geometry_1_jure.pdf}
\caption{The pinhole camera and the pinhole camera model. In the camera model
the image plane is in front of the camera center.}

\label{fig:pinhole_geometry_1}
\end{figure}

We assume that cameras follow the rules of the central projection, resulting in
the pinhole model, shown in Figure~\ref{fig:pinhole_geometry_1}. Furthermore,
we assume the center of projection is at $\mathbf{C} = (0, 0, 0)^T$ and that
the {\it image plane} is the plane $Z = f$.  The point $\mathbf{C}$ is called
the {\it camera center} or {\it optical center}. The line that passes through
the camera center and is perpendicular to the image plane is called the {\it
principal axis}. The point $\mathbf{p} = (0, 0, f)^T$, at which the principal
axis intersects the image plane is called the {\it principal point}.  
Under the central projection, a point in space with coordinates $\mathbf{X} =
(X, Y, Z)^T$ is mapped to the point where the line joining $\mathbf{C}$ and
$\mathbf{X}$ intersects the image plane. 
We derive the mapping from 3D scene points to 2D image points
by considering similar triangles in Figure~\ref{fig:pinhole_geometry_2}. The
mapping is given by:
%
\begin{equation}
(X, Y, Z)^T \mapsto (f X/Z, f Y/Z)^T.
\label{eqn:central_projection}
\end{equation}

In an actual camera, the image plane lies behind the camera center, but in this
work---and in computer vision in general---we place the image plane in front of
the camera center for mathematical convenience.

\begin{figure}[tb]
\includegraphics{img/pinhole_geometry_2_jure.pdf}
\caption{Using similar triangles to explain projection.}
\label{fig:pinhole_geometry_2}
\end{figure}

\subsubsection{Central Projection Using Homogeneous Coordinates}

When homogeneous coordinates are used to represent world and image points, the
mapping in Equation~\ref{eqn:central_projection} can be expressed as a central
projection,
%
\begin{equation}
\begin{pmatrix} X \\ Y \\ Z \\ 1 \end{pmatrix} \mapsto
\begin{pmatrix} fX \\ fY \\ Z \end{pmatrix} =
\begin{bmatrix} f & 0 & 0 & 0 \\ 0 & f & 0 & 0 \\ 0 & 0 & 1 & 0 \end{bmatrix}
\begin{pmatrix} X \\ Y \\ Z \\ 1 \end{pmatrix}.
\label{eqn:central_projection_2}
\end{equation}
%
The $3 \times 4$ projection matrix in Equation~\ref{eqn:central_projection_2} can be
written as the product of two matrices: $\text{diag}(f, f, 1)[I\text{ | }0]$,
where $\text{diag}(f, f, 1)$ is a $3 \times 3$ diagonal matrix and $[I \text{ |
} 0]$ is a $3 \times 4$ matrix composed of a $3 \times 3$ identity matrix and a
$3 \times 1$ vector of zeros.

Let $\mathbf{X} = (X, Y, Z, 1)^T$ denote the world point as a homogeneous
4-vector and let $\mathbf{x}$ denote the image point as a homogeneous 3-vector,
then the central projection under the basic pinhole model can be written as
%
\begin{equation}
\mathbf{x} = \text{diag}(f, f, 1)[I\text{ | }0] \mathbf{X}.
\end{equation}


\subsubsection{Principal Point Offset}

It is convenient to move the origin of the image plane away from the principal
point so that it corresponds with the lower-left corner of the image. We extend
the mapping from Equation~\ref{eqn:central_projection} and get the following
mapping:
%
\begin{equation}
(X, Y, Z)^T \mapsto (f X/Z + p_x, f Y/Z + p_y)^T,
\end{equation}
%
where $(p_x, p_y)^T$ is the new origin of the image plane. Using homogeneous
coordinates, we can express this mapping as a central projection,
%
\begin{equation}
\begin{pmatrix} X \\ Y \\ Z \\ 1 \end{pmatrix} \mapsto
\begin{pmatrix} fX + Z p_x \\ fY + Z p_y \\ Z \end{pmatrix} =
\begin{bmatrix} f & 0 & p_x & 0 \\ 0 & f & p_y & 0 \\ 0 & 0 & 1 & 0 \end{bmatrix}
\begin{pmatrix} X \\ Y \\ Z \\ 1 \end{pmatrix}.
\end{equation}
%
With the addition of the principal point offset, the projection matrix becomes
$P = K[I\text{ | }0]$, where the $3 \times 3$ matrix $K$, called the {\it
camera calibration matrix}, is defined as
%
\begin{equation}
K = \begin{bmatrix} f & 0 & p_x \\ 0 & f & p_y \\ 0 & 0 & 1 \end{bmatrix}.
\label{eqn:matrix_K1}
\end{equation}
%
The pinhole model with the addition of the principal point offset is, therefore, defined
as $\mathbf{x} = K[I\text{ | }0] \mathbf{X}$.

\subsubsection{Camera Rotation and Translation}

We have, thus far, assumed that the camera is located at the origin of a Euclidean coordinate
system with the principal coordinate pointing in the direction of the z-axis. We call
this coordinate system the {\it camera coordinate frame}. In general this assumption will not
hold and points in space will be represented in terms of a different coordinate
system, known as the {\it world coordinate frame}. The two coordinate frames
are related by a rotation and a translation:
%
\begin{equation}
\tilde{\pmb{X}}_\text{cam} = R (\tilde{\pmb{X}} - \tilde{C}),
\label{eqn:cam_to_world}
\end{equation}
%
where $\tilde{\pmb{X}}$ is a point in the world coordinate frame,
$\tilde{\pmb{X}}_\text{cam}$ is the same point in the camera coordinate frame,
$R$ is a $3 \times 3$ rotation matrix encoding the orientation of the camera
relative to the world coordinate frame and $\tilde{C}$ is the camera center in
the world coordinate frame. Vectors $\tilde{\pmb{X}}_\text{cam}$,
$\tilde{\pmb{X}}$, and $\tilde{C}$ are represented using inhomogeneous
3-vectors as indicated by the tilde sign above their names.
Equation~\ref{eqn:cam_to_world} can also be written in homogeneous coordinates:
%
\begin{equation}
\mathbf{X}_\text{cam} = \begin{bmatrix} R & -R \tilde{C} \\ 0 & 1 \end{bmatrix} \mathbf{X},
\end{equation}
%
and the projection is defined as
%
\begin{equation}
\mathbf{x} = K[I\text{ | }0] \mathbf{X}_\text{cam} = KR[I\text{ | }-\tilde{C}]\mathbf{X}.
\label{eqn:world2image}
\end{equation}
%
The three parameters contained in matrix $K$ are called the {\it internal
camera parameters}, while the parameters in $R$ and $\tilde{C}$ which give the
position and orientation of the camera relative to the world coordinate frame
are called {\it external camera parameters}.

\subsubsection{Digital Camera}

Since digital cameras may have rectangular, rather than square, pixels we need
to adjust our mathematical model accordingly. Let $m_x$ and $m_y$ be the
number of pixels per unit distance in the x and y direction, respectively. To
account for the uneven pixel scaling, matrix $K$, defined in
Equation~\ref{eqn:matrix_K1}, takes the form:
%
\begin{equation}
K = \begin{bmatrix} \alpha_x & 0 & x_0 \\ 0 & \alpha_y & y_0 \\ 0 & 0 & 1 \end{bmatrix},
\end{equation}
%
where $\alpha_x = f m_x$ and $\alpha_y = f m_y$ represent the focal lengths of
the digital camera in pixels. Likewise, $x_0 = m_x p_x$ and $y_0 = m_y p_y$
represent the camera center in pixels.

\subsubsection{Summary}

The mapping from world points to image points is modeled with the central
projection. Let $\mathbf{X}$ be homogeneous 4-vector, $\mathbf{x}$ be a
homogeneous 3-vector, and let $P$ be a $3 \times 4$ projection matrix. In the
process of image formation, the world point $\mathbf{X}$ is mapped to the image
points $\mathbf{x}$ in the following way:
%
\begin{equation}
\mathbf{x} = \mathtt{P} \mathbf{X}
= KR[I\text{ | }-\tilde{C}] \mathbf{X}
= \begin{bmatrix} \alpha_x & 0 & x_0 \\ 0 & \alpha_y & y_0 \\ 0 & 0 & 1 \end{bmatrix}
R[I\text{ | }-\tilde{C}]\mathbf{X}.
\end{equation}

% \mathbf{x} = K[I\text{ | }0] \mathbf{X}_\text{cam} = KR[I\text{ | }-\tilde{C}]\mathbf{X}.

\subsubsection{Lens Distortion}

Using an actual pinhole camera is not the best way to take images because the
amount of light that passes through the pinhole is small. A lens is typically
used in place of the pinhole, which increases the amount of light reaching the film.
The downside of using a lens is that it introduces distortions and the pinhole
camera model is no longer an accurate model of the image formation process. 

\begin{figure}[tb]

\begin{minipage}{\textwidth}
\center
\raisebox{-0.5\height}{\includegraphics[scale=0.3]{img/checkerboard_small.png}}
\hspace*{.2in}
\raisebox{-0.5\height}{\includegraphics[scale=0.32]{img/checkerboard_distorted_small.png}}
\end{minipage}

\caption{Barrel distortion is a lens effect that causes an image to appear as if
it has been wrapped around a sphere.}

\label{fig:distortion}
\end{figure}

The most important deviation from the pinhole model is caused by {\em radial
distortion}, where pixels near the boarder get distorted, causing a barrel
effect as seen in Figure~\ref{fig:distortion}. The distortion is zero near the
center of the image and increases towards the edges. Let $(x, y)$ denote the
image location of a 3D point $\mathbf{X}$ under the pinhole model and $(x_d,
y_d)$ denote the image location of the same 3D point $\mathbf{X}$ with
radial distortion. The relation between $(x, y)$ and $(x_d, y_d)$ is
described with the following equation:
%
\begin{equation} \begin{pmatrix}x_d \\ y_d\end{pmatrix} = \begin{pmatrix}x_c \\
y_c\end{pmatrix} + L(r) \begin{pmatrix}x - x_c \\ y - y_c\end{pmatrix},
\end{equation}
%
where $(x_c, y_c)$ denotes the center of the radial distortion; $r$ denotes the
euclidean distance of $(x, y)$ from the center of distortion, that is, $r^2 =
(x - x_c)^2 + (y - y_c)^2$; and $L(r)$ is typically defined as $L(r) = 1 +
\kappa_1 r + \kappa_2 r^2 + \kappa_3 r^3$. The coefficients $\kappa_1$,
$\kappa_2$, and $\kappa_3$ are determined---together with other internal camera
parameters---in the process of image calibration.

\subsection{Epipolar Geometry}

The {\em epipolar geometry} is the intrinsic projective geometry between two
views and depends only on the cameras' internal parameters and their relative
position and orientation. It is the geometry of the intersection of the image
planes with the set of planes containing the baseline, known as the
\emph{epipolar planes}. Studying epipolar geometry is of great importance for
solving the stereo correspondence problem, as it limits the search for
correspondences to a one dimensional search along a line.

\begin{figure}[tb]
\includegraphics{img/epipolar_plane.pdf}
\caption{The camera centers $\mathbf{C}$ and $\mathbf{C'}$, the point $\mathbf{X}$ and
its two images $\mathbf{x}$ and $\mathbf{x'}$ lie on the same plane, called the {\em
epipolar plane}.}
\label{fig:epipolar1}
\end{figure}

Two cameras, with camera centers $\mathbf{C}$ and $\mathbf{C'}$, are observing
a scene point $\mathbf{X}$. Let $\mathbf{x}$ be the image of point $\mathbf{X}$ in
the first camera and $\mathbf{x'}$ be the image of point $\mathbf{X}$ in the
second camera. We would like to know how $\mathbf{x}$ and $\mathbf{x'}$ are
related. As shown in Figure~\ref{fig:epipolar1}, all five points---the camera
centers $\mathbf{C}$ and $\mathbf{C'}$, the scene point $\mathbf{X}$ and its
two images $\mathbf{x}$ and $\mathbf{x'}$---lie on the same plane, known as the
{\em epipolar plane}. The point at which the baseline intersects the image
plane is called the {\em epipole}, denoted as $\mathbf{e}$ on the left and
$\mathbf{e'}$ on the right image plane.

\begin{figure}[tb]
\includegraphics{img/epipolar_plane2.pdf}
\caption{The epipolar line in the second view $\mathbf{l'}$ is defined as the
intersection of the second image plane and the epipolar plane, which is itself
defined by $\mathbf{x}$. This figure illustrates that, given point $\mathbf{x}$
its corresponding point $\mathbf{x'}$ has to lie on the epipolar line
$\mathbf{l'}$.}

\label{fig:epipolar2}
\end{figure}

\begin{figure}[tb]
\begin{tabular}{cc}
\includegraphics[scale=0.3]{img/left_e_small.jpg} \\
\includegraphics[scale=0.3]{img/right_e_small.jpg}
\end{tabular}

\caption{Two views of the Washington Square Arch with epipolar lines drawn
through five corresponding points.}

\label{fig:epipolar5}
\end{figure}

Consider the epipolar plane defined by $\mathbf{x}$, $\mathbf{C}$ and
$\mathbf{C'}$. We know that $\mathbf{x'}$ lies on the same epipolar plane. The
point $\mathbf{x'}$ is, therefore, constrained to lie on the intersection of
the epipolar plane and the image plane. The intersection of these two planes is
a line, known as the {\em epipolar line}. This result is important and worth
stating again. We assume that the internal camera parameters and the cameras'
relative position and orientation are known. Given a point $\mathbf{x}$ in the
first view we wish to find its corresponding point $\mathbf{x'}$ in the second
view. We have just shown that point $\mathbf{x'}$ is constrained to lie on the
epipolar line $\mathbf{l'}$ that depends only on $\mathbf{x}$. 

Another way to see this is illustrated in Figure~\ref{fig:epipolar2}. Given an
image point $\mathbf{x}$, the scene point $\mathbf{X}$ is constrained to lie on
the line that passes through $\mathbf{C}$ and $\mathbf{x}$. If we project
points from that line onto the second image plane, we observe that they all lie
on the epipolar line $\mathbf{l'}$. In fact, we can define the epipolar line
$\mathbf{l'}$ as the projection of the line that passes through $\mathbf{C}$
and $\mathbf{x}$ onto the second image plane.


An example of epipolar geometry on a pair of images is given in
Figure~\ref{fig:epipolar5}.

\subsection{The Fundamental Matrix}

In this section we introduce the fundamental matrix $\mathtt{F}$ that describes
the mapping from image points to epipolar lines. 
Consider the following two points: (1) $\mathbf{C}$, the first camera center,
and (2) $\mathtt{P}^+\mathbf{x}$, where $\mathbf{x}$ is a point on the image
plane of the first camera, $\mathtt{P}$ is the projection matrix of the first
camera, and $\mathtt{P}^+$ denotes its pseudo-inverse, that is $\mathtt{PP}^+ =
\mathtt{I}$. Both points lie on the line that passes through $\mathbf{C}$ and
$\mathbf{x}$. The first point, $\mathbf{C}$, does so by definition; the second,
$\mathtt{P}^+\mathbf{x}$, because it projects to the point $\mathbf{x}$, that
is, $\mathtt{PP}^+\mathbf{x} = \mathbf{x}$.

The projections of these two points in the second view lie on the epipolar line
$\mathbf{l'}$. Let $\mathtt{P}'$ be the projection matrix of the second camera,
then the two points are projected in the second view as $\mathtt{P}'\mathbf{C}$
and $\mathtt{P}'\mathtt{P}^+\mathbf{x}$. Note that $\mathtt{P}'\mathbf{C}$ is
equal to $\mathbf{e'}$---the epipole---because it is the image of the first
camera center in the second view. The epipolar line $\mathbf{l'}$ is defined
as
%
\begin{equation}
\mathbf{l}' = 
(\mathtt{P}'\mathbf{C}) \times (\mathtt{P}'\mathtt{P}^+\mathbf{x}) =
\mathbf{e}' \times (\mathtt{P}'\mathtt{P}^+\mathbf{x}).
\label{eqn:epipolar_line0}
\end{equation}
%
Recall, from Equation~\ref{eqn:line_passing}, that the line $\mathbf{l}$ joining
points $\mathbf{x}$ and $\mathbf{y}$ is defined as the cross product
$\mathbf{l} = \mathbf{x} \times \mathbf{y}$. We can write
Equation~\ref{eqn:epipolar_line0} as a single matrix-vector multiplication by
expanding the epipole $\mathbf{e'}$ into the $3 \times 3$ matrix
$[\mathbf{e'}]_\times$ (see Equation~\ref{eqn:cp_mv}). By doing so, we derive
$\mathbf{l}' = [\mathbf{e}']_\times \mathtt{P}'\mathtt{P}^+\mathbf{x}$ or
%
\begin{equation}
\mathbf{l}' = \mathtt{F} \mathbf{x},
\label{eqn:fm}
\end{equation}
%
where $\mathtt{F} = [\mathbf{e}']_\times \mathtt{P}'\mathtt{P}^+$ is the {\em
fundamental matrix}---the $3 \times 3$ matrix that maps points to epipolar
lines. 
% Because $\text{rank}([\mathbf{e}']_\times) = 2$ and
% $\text{rank}(\mathtt{P}') = \text{rank}(\mathtt{P}^+) = 3$, the fundamental
% matrix has rank 2 and is, therefore, not invertible. It follows that
% $\det(\mathtt{F}) = 0$.

The fundamental matrix is homogeneous: 
%
\begin{equation}
\mathtt{F} \sim k\mathtt{F}\text{ for }k \neq 0.
\label{eqn:homo_fundamental}
\end{equation}
%
That is, $\mathtt{F}$ and $k\mathtt{F}$ are equivalent; they both produce the
same mapping from points to lines. This is true because $\mathtt{F} \mathbf{x}
= \mathbf{l}'$, $k \mathtt{F} \mathbf{x} = k \mathbf{l}'$, and since lines are
given as homogeneous 3-vectors, lines $\mathbf{l}'$ and $k \mathbf{l}'$
represent the same line.

% \paragraph{Degrees of freedom}
% 
% A general $3 \times 3$ matrix has 9 degrees of freedom, one for each matrix
% element. The fundamental matrix, however, has only 7 degrees of freedom. One
% degree of freedom is lost because it is defined only up to scale (see
% Equation~\ref{eqn:homo_fundamental}), and the other because $\det(\mathtt{F}) =
% 0$.

\subsubsection{Correspondence Condition}

% Equation~\ref{eqn:fm} shows that the point $\mathbf{x}$ and the epipolar line $\mathbf{l}'$
% are connected via the fundamental matrix $\mathtt{F}$; given $\mathbf{x}$ the epipolar line
% $\mathbf{l}'$ is computed simply as the matrix-vector product $\mathtt{F} \mathbf{x}$.

Because point $\mathbf{x}'$ lies on the epipolar line $\mathbf{l}' = \mathtt{F} \mathbf{x}$,
it follows from Equation~\ref{eqn:point_line} that
%
\begin{equation}
\mathbf{x}'^T \mathtt{F} \mathbf{x} = 0,
\label{eqn:xFx}
\end{equation}
%
that is, points $\mathbf{x}$ and $\mathbf{x}'$ correspond if and only if
$\mathbf{x}'^T \mathtt{F} \mathbf{x} = 0$. Equation~\ref{eqn:xFx} characterizes
the fundamental matrix $\mathtt{F}$ in terms of point correspondences,
without reference to the camera projection matrices $\mathtt{P}$ and
$\mathtt{P'}$. This enables the fundamental matrix $\mathtt{F}$ to be estimated
from image correspondences alone.

\subsubsection{Fundamental Matrix for a Calibrated Camera Stereo Rig}

Suppose we have two cameras with the following projection matrices: $\mathtt{P}
= \mathtt{K}[\mathtt{I}\text{ | }\mathbf{0}]$ and $\mathtt{P}' =
\mathtt{K'}[\mathtt{R}\text{ | }\mathbf{t}]$; that is, the first camera
center is located at the world origin with its principal ray equal to the
z-axis. The camera center $\mathbf{C}$ and the pseudo-inverse of the
projection matrix $\mathtt{P}^+$ are defined as
%
\begin{equation*}
\mathbf{C} = \begin{pmatrix} \mathbf{0} \\ 1 \end{pmatrix}
\text{ and }
\mathtt{P}^+ = \begin{bmatrix} \mathtt{K}^{-1} \\ \mathbf{0}^T \end{bmatrix}.
\end{equation*}
%
We can write the fundamental matrix $\mathtt{F}$ in terms of the cameras'
internal parameters and their relative position and orientation as
%
\begin{equation}
\mathtt{F} = [\mathbf{e}']_\times \mathtt{P}' \mathtt{P}^+ = 
[\mathtt{P}' \mathbf{C}]_\times \mathtt{P}' \mathtt{P}^+ =
[\mathtt{K}' \mathbf{t}]_\times \mathtt{K}' \mathtt{R} \mathtt{K}^{-1}
\end{equation}

\subsubsection{Fundamental Matrix for a Parallel Camera Stereo Rig} 

Assume the cameras of a stereo rig have identical internal parameters, that is,
$\mathtt{K}' = \mathtt{K} = \text{diag}(f, f, 1)$; both cameras axis are
aligned with the z-axis, that is, $\mathtt{R} = \mathtt{I}$; and the camera
centers differ only in the x-coordinate, that is, $\mathbf{t} = (t_x, 0, 0)^T$.
The fundamental matrix $\mathtt{F}$ can, then, be expressed as
%
\begin{multline*}
\mathtt{F}
= [\mathtt{K}' \mathbf{t}]_\times \mathtt{K}' \mathtt{R} \mathtt{K}^{-1}
= [\mathtt{K} \mathbf{t}]_\times \mathtt{K} \mathtt{I} \mathtt{K}^{-1}
= [\mathtt{K} \mathbf{t}]_\times = \\
= \begin{bmatrix}f t_x \\ 0 \\ 0 \end{bmatrix}_\times 
= \begin{bmatrix}0 & 0 & 0 \\ 0 & 0 & -f t_x \\ 0 & f t_x & 0 \end{bmatrix}
\sim \begin{bmatrix}0 & 0 & 0 \\ 0 & 0 & -1 \\ 0 & 1 & 0 \end{bmatrix}.
\end{multline*}
%
The last step of the derivation follows from
Equation~\ref{eqn:homo_fundamental}.

To see how this special form of the fundamental matrix restricts the search
for correspondences we expand Equation~\ref{eqn:xFx}.
%
\begin{align*}
\mathbf{x}'^T \mathtt{F} \mathbf{x} &= 0 \\
\begin{pmatrix} x' & y' & 1 \end{pmatrix}
\begin{bmatrix}0 & 0 & 0 \\ 0 & 0 & -1 \\ 0 & 1 & 0 \end{bmatrix}
\begin{pmatrix} x \\ y \\ 1 \end{pmatrix} & = 0 \\
-y' + y &= 0 \\
y &= y'.
\end{align*}
%
If points $\mathbf{x} = (x, y)^T$ and $\mathbf{x}' = (x', y')^T$ correspond it
follows that $y = y'$, or, in other words, the epipolar line of $\mathbf{x} =
(x, y)^T$ is the horizontal line $y' = y$. Aligning the epipolar lines in this
way simplifies the implementation of stereo algorithms, because the search for
corresponding points is carried out along horizontal lines. Unfortunately,
constructing a perfect parallel stereo rig is nearly impossible in practice,
but we can correct for the mechanical misalignments by transforming the two
views in a process known as {\em image rectification}.

Image rectification is the process of resampling pairs of stereo images so that
all epipolar lines are horizontal and corresponding points have identical
y-coordinates. A pair of projective transformations are applied to the two
images in order to match the epipolar lines. Since there are many projective
transformation that achieve this, a typical rectification method attempts to
find a pair of transformations that subject the images to minimal distortion.
After image rectification, the epipolar geometry is the same as if the images
were taken from a parallel stereo rig. Because most stereo methods assume this
to be the case, image rectification is often performed before the stereo method.


\subsubsection{Depth and Disparity}

\begin{figure}[t]
\includegraphics{img/disparity.pdf}
\caption{Depth and disparity are inversely proportional.}

\label{fig:disparity}
\end{figure}

Depth and disparity are inversely proportional as we saw in
Equation~\ref{eqn:disparity}. We derive this relation in this
section using similar triangles.

Consider a parallel camera stereo rig shown in Figure~\ref{fig:disparity}.
$C_L$ and $C_R$ denote the camera centers, $X$ denotes a 3D scene point that
projects to $x_L$ and $x_R$ in the left and right image planes. Let $f$ denote
the focal length, $B$ the baseline, $d$ the disparity and $z$ the depth of
point $X$. The disparity $d$ is defined as the difference in horizontal
location of $x_L$ in the left and $x_R$ in the right image, that is, $d = p_x -
p_y$. Triangles $\Delta C_L X C_R$ and $\Delta x_L X x_R$ are similar
triangles, therefore:
%
\begin{equation}
\frac{B}{z} = \frac{B - d}{z - f} \implies z = \frac{f B}{d},
\end{equation}
%
which proves Equation~\ref{eqn:disparity}.
