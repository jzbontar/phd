\section{Stereo Vision}

Early work on computational stereo started in the 1970s by the Image
Understanding community. \citet{barnard1982computational} reviewed stereo
research up to 1981. Their paper focuses on the fundamentals of computational
stereo (image acquisition, camera modeling, feature acquisition, matching,
distance determination, and interpolation), evaluation criteria, and contains
a survey of a representative sampling of the Image Understanding work relevant
to computational stereo. Work on stereo continued in the 1980s, with a review
paper published in 1989 by~\citet{dhond1989structure}, which surveys many new
stereo matching methods. It also introduces hierarchical processing and the use of
trinocular constraints. The two most recent review papers on the topic
are~\citet{brown2003advances} from 2003, which focuses on correspondence
methods, methods for dealing with occlusion, and real-time implementations; and
\citet{scharstein2002taxonomy} from 2002, which provides a taxonomy and
categorization of existing stereo algorithms allowing individual components of
the stereo algorithm to be compared. A good overview of stereo correspondence
algorithms is also given in the book by~\citet{szeliski2010computer}.

In this section, we focus on methods that operate on two rectified images and
produce a dense disparity map. According to~\citet{scharstein2002taxonomy}
stereo algorithms perform a subset of the following four steps: (1) matching
cost computation, (2) cost aggregation, (3) disparity computation, and (4)
disparity refinement. To see how a stereo algorithm decomposes into the four
steps, consider a simple stereo algorithm with a {\em sum of absolute
differences} matching cost and a winner-takes-all strategy. (1) The matching
cost is computed as the absolute difference between the intensity values of a
pixel in the left and a pixel in the right image. (2) The cost is aggregated by
computing the average matching cost within a small rectangular window of
neighbouring pixels. (3) The disparity is computed by selecting the disparity
that minimizes the aggregated cost.

Some methods are defined on small image patches and perform steps (1) and (2)
jointly. Examples include normalized cross-correlation~\cite{bolles1993jisct,
hannah1974computer}, the rank and census transforms~\citet{zabih1994non}, and
our method based on convolutional neural networks~\cite{Zbontar_2015_CVPR}.

Methods which compute disparity by considering only a small neighbourhood
around the pixel of interest are know as \emph{local} algorithms. {\em Global}
algorithms, on the other hand, take the whole image into account, typically by
making explicit smoothness assumptions and solving an optimization problem.
Global algorithms usually omit the cost aggregation step as the information
from neighbouring pixels is propagated during the optimization step. Global
methods define a cost function, which usually contains a data term and a
smoothness term. Common optimization algorithms include 
iterated conditional modes~\cite{besag1986statistical},
simulated annealing~\cite{kirkpatrick1984optimization, barnard1989stochastic, marroquin1987probabilistic},
probabilistic diffusion~\cite{scharstein1998stereo}, 
graph cuts~\cite{boykov2001fast, kolmogorov2004energy}, and 
message passing algorithms~\cite{yedidia2000generalized, weiss2001correctness, kolmogorov2006convergent}. 
Soma algorithms are difficult to
classify as either local or global. Certain iterative algorithms, for example,
do not explicitly state their global cost function, but their iterative
behaviour is similar to some optimization algorithms~\cite{marr1976cooperative,
scharstein1998stereo, zitnick2000cooperative}. Hierarchical algorithms resemble
such iterative algorithms. They operate on a pyramid of images, solving the
correspondence problem at a coarse level first and refine their solution at
each successive step~\cite{bergen1992hierarchical, quam1984hierarchical,
witkin1987signal, eigen2014depth, eigen2015predicting, mayer2015large}.

\subsection{Matching Costs}

Since the main contribution of this thesis is a new matching cost, we review
existing matching cost functions in great detail. 

A matching cost $C(\mathbf{p}, d)$ is a function that maps a position
$\mathbf{p}$ and a disparity $d$ into a real number, which is interpreted as
the cost of matching position $\mathbf{p}$ in the left image with position
$\mathbf{p} - \mathbf{d}$ in the right image. We want the cost to be lowest
when the left and right positions correspond. We use bold lowercase letters
$\mathbf{p}$ and $\mathbf{q}$ to denote image locations. A bold lowercase
$\mathbf{d}$ denotes the disparity $d$ cast to a vector, that is, $\mathbf{d} =
(d, 0)$.


Simple matching costs assume constant intensities at matched locations, but
more advanced matching costs are more robust and can compensate for certain
radiometric differences and noise. Radiometric differences are caused, for
example, due to slightly different camera settings, vignetting, and image
noise. Reducing the stereo baseline would reduce these differences, but would
also reduce the geometric accuracy of triangulation. The following section
describes several matching costs as they were presented by
\citet{hirschmuller2009evaluation}. They group the matching costs into three
categories: parametric matching costs, non-parametric matching costs, and
mutual information.

\subsubsection{Parametric Matching Costs}

Parametric costs use the magnitude of the pixel intensities.

\paragraph{Absolute differences}
Probably the simplest parametric matching cost is the {\em absolute
differences}, which assumes brightness consistency for corresponding pixels,
that is, it assumes the intensity values of corresponding pixels are the same.
The absolute differences matching cost $C_{\text{AD}}$ is defined as
%
\begin{equation}
C_{\text{AD}}(\mathbf{p}, d) = | I^L(\mathbf{p}) - I^R(\mathbf{p - d}) |
\end{equation}
%
where $I^L(\mathbf{p})$ and $I^R(\mathbf{p})$ are image intensities at position
$\mathbf{p}$ in the left and right image, respectively.  

\paragraph{Sum of absolute differences}
Local stereo methods typically aggregate this matching cost over all pixels within
a local neighbourhood. This is referred to as the sum of absolute differences
matching cost $C_{\text{SAD}}$ and is defined as
%
\begin{equation} 
C_{\text{SAD}}(\mathbf{p}, d) = \sum_{\mathbf{q} \in N_{\mathbf{p}}} |I^L(\mathbf{q}) - I^R(\mathbf{q - d})|,
\label{eqn:sum_of_absolute_differences}
\end{equation}
%
where $N_{\mathbf{p}}$ is the set of locations within a rectangular window
centered at $\mathbf{p}$.

\begin{figure}[tb]
\includegraphics[scale=0.3]{img/bt_sampling.png}
\caption{Two scanlines differing by a disparity of $0.4$. Image taken from
\cite{birchfield1998pixel}.}

\label{fig:bt_sampling}
\end{figure}

\paragraph{Birchfield-Tomasi}
Image sampling can cause traditional matching costs to assign high values to a
corresponding pair of pixels even in the absence of noise.
\citet{birchfield1998pixel} propose a method for measuring the matching cost
that is insensitive to image sampling. 

Let $i^L$ and $i^R$ be one-dimensional continuous intensity functions. The
functions $i^L$ and $i^R$ are sampled at discrete points by the image sensors,
resulting in two discrete one-dimensional arrays of image intensity values
$I^L$ and $I^R$, as shown in Figure~\ref{fig:bt_sampling}. Let $\hat{I}^L$ and
$\hat{I}^R$ denote the linearly interpolated functions between the sample
points of $I^L$ and $I^R$. The Birchfield-Tomasi cost function is defined as
%
\begin{align}
C_{\text{BT}}(\mathbf{p}, d) = \min\Big\{ & 
  \min_{ -0.5 \leq d' \leq 0.5}{I^L(\mathbf{p}) - \hat{I}^R(\mathbf{p - d} + (d', 0)^T)}, \nonumber \\
& \min_{ -0.5 \leq d' \leq 0.5}{\hat{I}^L(\mathbf{p} + (d', 0)^T) - I^R(\mathbf{p - d})}\Big\}.
\end{align}
%
Since the extreme points of a piecewise linear function must be its breakpoints, the
computation of the Birchfield-Tomasi cost can be computed efficiently using
the following equations:
%
\begin{align}
C_{\text{BT}}(\mathbf{p}, d) &= \min(A, B) \\
A &= \max(0, I^L(\mathbf{p}) - I^R_{\max}(\mathbf{p - d}), I^R_{\min}(\mathbf{p - d}) - I^L(\mathbf{p})) \nonumber \\
B &= \max(0, I^R(\mathbf{p - d}) - I^L_{\max}(\mathbf{p}), I^L_{\min}(\mathbf{p}) - I^R(\mathbf{p - d})) \nonumber \\
I_{\min}(\mathbf{p}) &= \min(I_-(\mathbf{p}), I(\mathbf{p}), I_+(\mathbf{p})) \nonumber \\
I_{\max}(\mathbf{p}) &= \max(I_-(\mathbf{p}), I(\mathbf{p}), I_+(\mathbf{p})) \nonumber \\
I_-(\mathbf{p}) &= (I(\mathbf{p} - (1, 0)^T) + I(\mathbf{p})) / 2 \nonumber \\
I_+(\mathbf{p}) &= (I(\mathbf{p} + (1, 0)^T) + I(\mathbf{p})) / 2 \nonumber
\end{align}
%
Another window-based sampling-insensitive cost function was developed
by~\citet{vcech2005complex}.

\begin{figure}[tb]
\includegraphics[scale=0.23]{img/mc_filters.png}
\caption{Different filters on the Teddy stereo image pair. Image taken from
\cite{hirschmuller2009evaluation}.}

\label{fig:mc_filters}
\end{figure}

The next three matching costs are filters that are applied to the two images
separately before computing the matching cost with absolute differences.

\paragraph{Mean filter}
The {\em mean filter} subtracts, at each location of the image, the mean of
the intensity values within a square window centered at the pixel of interest.
A constant offset of 128 is usually added to avoid negative numbers when
using unsigned 8-bit variables. The mean filter is defined as
%
\begin{equation}
I_{\text{mean}}(\mathbf{p}) = I(\mathbf{p}) - \frac{1}{|N_{\mathbf{p}}|} \sum_{\mathbf{q} \in N_{\mathbf{p}}} I(\mathbf{q}) + 128.
\label{eqn:mean_filter}
\end{equation}

\paragraph{Laplacian of Gaussian}
The {\em Laplacian of Gaussian} (LoG) filter performs smoothing for removing
noise and removes an offset in intensities. It is computed by convolving the
image with a LoG kernel,
%
\begin{align}
I_{\text{LoG}} &= I \ast K_{\text{LoG}} \\
K_{\text{LoG}}(x, y) &= -\frac{1}{\pi\sigma^4} \left( 1 - \frac{x^2 + y^2}{2 \sigma^2} \right) e^{\frac{x^2 + y^2}{2 \sigma^2}} \nonumber,
\end{align}
%
where $\sigma$ is the standard deviation. The LoG filter is often used in
real-time methods~\cite{konolige1998small,hirschmuller2002real}.

\paragraph{Background subtraction by bilateral filtering} The final filter we
consider is {\em background subtraction by bilateral filtering} (BilSub)
developed by~\citet{ansar2004enhanced}. The bilateral
filter~\cite{tomasi1998bilateral} has the effect of a gaussian blur without
blurring high contrast texture. It sums intensity values of neighboring pixels
weighted according to proximity and color similarity. The BilSub filter is
computed by subtracting, at each position, the value of the bilateral filter at
that location:
%
\begin{align}
I_{\text{BilSub}}(\mathbf{p}) &= I(\mathbf{p}) - \frac{\sum_{\mathbf{q} \in N_{\mathbf{q}}} I(\mathbf{p}) e^s e^r}{\sum_{\mathbf{q} \in N_{\mathbf{q}}} e^s e^r}, \\
s &= -\frac{\|\mathbf{q} - \mathbf{p}\|}{2 \sigma_s^2}, \nonumber \\
r &= -\frac{(I(\mathbf{q}) - I(\mathbf{p}))^2}{2 \sigma_r^2}. \nonumber
\end{align}
%
The parameter $\sigma_s$ controls the amount of smoothing, while the parameter
$\sigma_r$ prevents smoothing over high-contrast regions.

\paragraph{Zero-mean sum of absolute differences}
The {\em zero-mean sum of absolute differences} (ZSAD) subtracts the mean
intensity value of a window from every pixel in that windows and computes
the sum of absolute differences.
%
\begin{align}
C_{ZSAD}(\mathbf{p}, d) &= \sum_{\mathbf{q} \in N_{\mathbf{p}}} | I^L(\mathbf{q}) - \bar{I}^L(\mathbf{p}) - I^R(\mathbf{q - d}) + \bar{I}^R(\mathbf{p - d}) | \\
\bar{I}(\mathbf{p}) &= \frac{1}{N_\mathbf{p}} \sum_{\mathbf{q} \in N_{\mathbf{p}}} I(\mathbf{q}) \nonumber
\end{align}
%
In zero-mean sum of absolute differences the same mean value is subtracted at
every location in the window. Note that this is similar, but not quite the same,
as applying the mean filter from Equation~\ref{eqn:mean_filter} followed by the
sum of absolute differences cost, where the mean is computed for every pixel
individually.

\paragraph{Normalized cross-correlation} Normalized cross-correlation (NCC)
is a window-based method that is commonly used in practice. It can compensate
for gain changes but tends to work poorly on depth discontinuities, because
outliers are strongly penalized. The normalized cross-correlation $C_{\text{NCC}}$
is defined as:
%
\begin{equation}
C_{NCC}(\mathbf{p}, d) = \frac{\sum_{\mathbf{q} \in N_{\mathbf{p}}} I^L(\mathbf{q})I^R(\mathbf{q - d})}
{\sqrt{\sum_{\mathbf{q} \in N_{\mathbf{p}}} I^L(\mathbf{q})^2 \sum_{\mathbf{q} \in N_{\mathbf{p}}} I^R(\mathbf{q - d})^2 }} 
\label{eqn:ncc}
\end{equation}
%
A variant of normalized cross-correlation was presented by \citet{moravec1977}.
It only approximates Equation~\ref{eqn:ncc}, but is faster to compute.

\paragraph{Zero-mean normalized cross-correlation} The last parametric matching
cost is the zero-mean normalized cross-correlation (ZNCC). It is similar to NCC
but can additionally compensate for differences in offset within the
correlation window. The zero-mean normalized cross-correlation cost $C_{\text{ZNCC}}$
is defined as:
%
\begin{equation}
C_{ZNCC}(\mathbf{p}, d) = \frac{\sum_{\mathbf{q} \in N_{\mathbf{p}}} (I^L(\mathbf{q}) - \bar{I}^L(\mathbf{p}) ) (I^R(\mathbf{q - d}) - \bar{I}^R(\mathbf{p - d}) )}
{\sqrt{\sum_{\mathbf{q} \in N_{\mathbf{p}}} (I^L(\mathbf{q}) - \bar{I}^L(\mathbf{p}))^2 
\sum_{\mathbf{q} \in N_{\mathbf{p}}} (I^R(\mathbf{q - d}) - \bar{I}^R(\mathbf{p - d}))^2 }} 
\label{eqn:zncc}
\end{equation}

\subsubsection{Non-parametric Matching Costs}

Whereas parametric matching costs used pixel intensity values, non-parametric
matching costs use only their local ordering. Most of them can be implemented
as filters.

\paragraph{Rank filter}

The Rank filter is defined with the following equation:
%
\begin{equation}
I_{\text{Rank}}(\mathbf{p}) = \sum_{\mathbf{q} \in N_{\mathbf{p}}} 1\{I(\mathbf{q}) < I(\mathbf{p}) \},
\label{eqn:rank}
\end{equation}
%
where $1\{\cdot\}$ denotes the indicator function. The Rank filter at position
$\mathbf{p}$ is defined as the number of positions $\mathbf{q}$ in some region
of interest that have a smaller intensity value than $\mathbf{p}$. It was
proposed by \citet{zabih1994non} to increase robustness to outliers within
the region of interest. Outliers frequently occur near depth discontinuities.
Since the rank filter only depends on the ordering of image intensity values
and not on the actual values, it is invariant to all transformations that
preserve the ordering. When computing the matching cost, the rank-transformed
images are compared using the absolute differences method.

\paragraph{Soft Rank filter}

Since the Rank filter uses only the ordering of intensity values, it is
sensitive in areas with low texture. Instead of always making hard decision
when comparing image intensity values as in Equation~\ref{eqn:rank}, it might
seem reasonable to soften the decision when positions $\mathbf{p}$ and
$\mathbf{q}$ have similar image intensity values. The Soft Rank filter defines
a linear---soft transition---zone between 0 and 1 for similar intensity values.
It is defined with the following equation:
%
\begin{equation}
I_{\text{SoftRank}}(\mathbf{p}) = \sum_{\mathbf{q} \in N_{\mathbf{p}}}
\min \left( 1, \max \left(0, \frac{I(\mathbf{p}) - I(\mathbf{q})}{2t} + \frac{1}{2} \right) \right),
\end{equation}
%
where $t$ is the hyperparameter of the filter that controls the size of the
transition zone.

\paragraph{Census filter}
The census filter~\citep{zabih1994non} represents each image position as a bit
vector. The vector is computed by cropping an image patch centered around the
position of interest and comparing the intensity values of each pixel in the
patch to the intensity value of the pixel in the center. When the center pixel
is darker the corresponding bit is set. Let $D$ be the set of displacements and
let $\bigotimes$ denote concatenation. The census transformed is defined as
%
\begin{equation}
I_{\text{Census}}(\mathbf{p}) = \bigotimes_{[i,j] \in D} 1\{I(\mathbf{p}) < I(\mathbf{p} + (i, j)^T)\}.
\label{eqn:census}
\end{equation}
%
The matching cost is defined as the hamming distance between two census
transformed vectors. The performance of the Census filter is better than the
Rank filter, but the computation costs are higher due to the calculation of the
hamming distance~\cite{zabih1994non}.

\paragraph{Ordinal measure}

The ordinal measure was proposed by~\citet{bhat1997motion}. It is based on the
distance of rank permutations of corresponding matching windows. It can't be
implemented as a filter and requires window-based matching.

\subsubsection{Mutual Information}

Mutual information was popularized in computer vision
by~\citet{viola1997alignment}. It is primarily used for image registration.
Mutual information of images $I_1$ and $I_2$ is defined as the sum of entropies
of the individual images, $H_{I_1}$ and $H_{I_2}$, minus their joint entropy,
$H_{I_1,I_2}$:
%
\begin{equation}
\text{MI}_{I_1,I_2} = H_{I_1} + H_{I_2} - H_{I_1,I_2}.
\end{equation}
%
The entropies are computed from probability distributions $P_{I_1}$, $P_{I_2}$,
and $P_{I_1,I_2}$, which are derived by normalizing the histograms of image
intensity values.
%
\begin{align}
H_I &= - \int_0^1 P_I(i) \log P_I(i)\,di, \\
H_{I_1,I_2} &= - \int_0^1 \int_0^1 P_{I_1,I_2}(i_1,i_2) \log  P_{I_1,I_2}(i_1,i_2) \,di_1 \,di_2.
\label{eqn:H12}
\end{align}
%
The mutual information of two images will be lower when they are well registered
than when they are not. The entropies $H_{I_1}$ and $H_{I_2}$ will not change, but the
joint entropy $H_{I_1,I_2}$ will be lower when the images are well registered.

If small image patches are used to compute mutual information there might not
be enough pixels to reliably estimate the probability distribution. If,
however, large image patches are used, the disparity maps will not be accurate
around depth discontinuities. There is a way to compute mutual information
based on the whole image and to allow pixel-wise matching~\cite{kim2003visual,
hirschmuller2008stereo}. \citet{kim2003visual} suggest an iterative algorithm
that begins with an initial disparity map and refines it at each step. The
initial disparity map is either random or is obtained from another stereo
matching cost. The initial disparity map is used to warp the second image. We
first compute the joint entropy, defined in Equation~\ref{eqn:H12}, as a sum of
terms over all image positions $\mathbf{p}$.
%
\begin{align}
H_{I_1,I_2} &= \sum_{\mathbf{p}} h_{I_1,I_2}(I_1(\mathbf{p}), I_2(\mathbf{p})), \\
h_{I_1,I_2}(i,k) &= -\frac{1}{n} \log (P_{I_1,I_2}(i,k) \ast g(i,k)) \ast g(i,k),
\end{align}
%
where $n$ denotes the number of pixels in the image and $\ast g(\cdot)$ denotes
convolution with a gaussian filter. See~\citet{kim2003visual} for the
derivation of this step. The probability distribution $P_{I_1,I_2}$ is defined as
%
\begin{equation}
P_{I_1,I_2}(i,k) = \frac{1}{n} \sum_{\mathbf{p}} 1\{(i,k) = (I_1(\mathbf{p}), I_2(\mathbf{p}))\}.
\end{equation}
%
The entropies of the individual images are computed analogously:
%
\begin{align}
H_I &= \sum_{\mathbf{p}} h_I(I(\mathbf{p})), \\
h_I(i) &= -\frac{1}{n} \log (P_I(i) \ast g(i)) \ast g(i).
\end{align}
%
The resulting definition of mutual information is:
%
\begin{align}
\text{MI}_{I_1,I_2} &= \sum_{\mathbf{p}} \text{mi}_{I_1, I_2}(I_1(\mathbf{p}), I_2(\mathbf{p})), \\
\text{mi}_{I_1, I_2}(i, k) &= h_{I_1}(i) + h_{I_2}(k) - h_{I_1, I_2}(i, k).
\end{align}
%
Which leads to the definition of the mutual information matching cost function,
%
\begin{equation}
C_{\text{MI}}(\mathbf{p}, d) = -\text{mi}_{I^L, f_D(I^R)}(I^L(\mathbf{p}), I^R(\mathbf{p - d})).
\end{equation}

The method is usually performed in a hierarchical
fashion~\cite{hirschmuller2008stereo}, by downscaling the image by a factor of
16. After a number of iterations on the small resolution images, the obtained
disparity map is upsampled and used as the initial disparity estimate for
matching at $\frac{1}{8}$th of the full resolution. This process is repeated
until we reach the full resolution images. The disparity maps obtained at a
lower resolution are not used to constrain the search on the higher resolution,
but just as initial estimates of the disparity map.


\subsection{Stereo Methods}

Many problems in early vision, including stereo, can be viewed as assigning
labels to pixels. In the case of stereo, the labels represent the disparity at a
particular pixel. Pixel labelling problems are often solved by minimizing
an energy function that contains the following two terms: a data term, which
penalizes solutions that are inconsistent with local observations, and a
smoothness term, which enforces some kind of spatial coherence. This type of
energy function can be justified in terms of maximum a posteriori estimation of
a Markov Random Field~\cite{geman1984stochastic, besag1986statistical}. Wide
spread adoption of energy minimization approaches was slow at first, likely due
to inefficient optimization algorithms, such as iterated conditional
modes~\cite{besag1986statistical} and simulated
annealing~\cite{kirkpatrick1984optimization,barnard1989stochastic}. With the
introduction of powerful energy minimization algorithms, such as graph
cuts~\cite{boykov2001fast, kolmogorov2004energy} and loopy belief
propagation~\cite{yedidia2000generalized, weiss2001correctness}, the popularity
of energy minimization approaches grew. This section is modeled after the
comparative study of different energy minimization methods by
\citet{szeliski2006comparative}.

The pixel labeling problem is defined as assigning a label $l_{\mathbf{p}}$ to
each pixel $\mathbf{p}$ of an image. The number of pixels in an images is $n$, and
the number of labels is $m$. The energy function $E$ is written as
%
\begin{equation}
E = E_d + \lambda E_s,
\label{eqn:mrf_energy}
\end{equation}
%
where $E_d$ is the data term, $E_s$ is the smoothness term, and $\lambda$ is a
real number. The data term $E_d$ is the sum of a set of per-pixel data costs
$d_\mathbf{p}(l_{\mathbf{q}})$,
%
\begin{equation}
E_d = \sum_{\mathbf{p}} d_{\mathbf{p}}(l_{\mathbf{q}}).
\end{equation}
%
We assume that pixels form a $2$D grid. Therefore, a position $\mathbf{p}$ can
be written in terms of its coordinates $\mathbf{p} = (i, j)$. Furthermore, we
assume the standard 4-connected neighbourhood, where each pixels is connected
to its top, bottom, left, and right neighbour, that is, pixels $\mathbf{p} =
(i, j)$ and $\mathbf{q} = (s, t)$ are connected if and only if $|i - s| + |j -
t| = 1$. Let $N$ denote the set of all neighbouring pixel pairs. We can write
the smoothness term $E_s$ as the sum of spatially varying costs
$V_\mathbf{pq}(l_\mathbf{p}, l_\mathbf{p})$,
%
\begin{equation}
E_s = \sum_{\{\mathbf{p},\mathbf{q}\} \in N} V_\mathbf{pq}(l_\mathbf{p}, l_\mathbf{q}).
\end{equation}
%
where the notation $\{\mathbf{p},\mathbf{q}\}$ denotes unordered sets.

% A more restricted form of the smoothness term is
% %
% \begin{equation}
% E_s = \sum_{\{\mathbf{p},\mathbf{q}\} \in N} w_{\mathbf{pq}} V(|l_\mathbf{p} - l_\mathbf{q}|),
% \end{equation}
% %
% where $w_{\mathbf{pq}}$ is a spatially varying weight and $V(|l_\mathbf{p} -
% l_\mathbf{q}|) = V(\Delta l)$ a non-decreasing function of the label
% difference. We parameterize the function $V$ using a clipped monomial form,
% %
% \begin{equation}
% V(\Delta l) = \min(|\Delta l|^k, V_{\max}),
% \end{equation}
% %
% with $k$ set to either $1$ or $2$. If we set $V_{\max} = 1$, we get the Potts
% model, $V(\Delta l) = 1\{\Delta l \geq 1\}$, where the smoothness term is zero
% when the disparities of neighboring pixels are the same and one when the
% disparities of neighboring pixels differ. 

There are special cases of energy functions that have an algorithm for
computing the exact solution in polynomial time, unfortunately, none of them
will be useful for the problem of stereo matching. If there are only two
labels, the Potts model can be solved exactly by graph cuts. This was first
observed in the context of Scheduling by \citet{stone1977multiprocessor} and in
was first applied to images by \citet{greig1989exact}. If the labels are
integers starting with $0$ and the smoothness cost is an arbitrary convex
function, \citet{ishikawa2003exact} gives a graph cut construction. If
$V(\Delta l) = \Delta l$ and the data costs are convex, an algorithm due
to \citet{hochbaum2001efficient} can be used. The NP-hardness result proved by
\citet{boykov2001fast} holds if there are more than two labels, as long as the
class of smoothness costs include the Potts model.

\subsubsection{Iterated Conditional Modes (ICM)}

Iterated conditional modes~\cite{besag1986statistical} is a greedy algorithm
for find a local minimum of the energy function. It starts with an initial
assignment of labels to pixels. The initial assignment can be random. Better
results are achieved, however, if the initial assignment respects the data term
of the energy function. This is achieved by finding the assignment of labels to
pixels with $\lambda$ in Equation~\ref{eqn:mrf_energy} set to $0$. ICM is
extremely sensitive to the initial estimate, especially in high-dimensional
spaces with non-convex energies due to the large number of local minima. 

ICM works by repeatedly applying the following step: for each pixel, set its
label to the one that gives the largest decrease in energy. The updates can be
performed asynchronously, with pixels being updated one after the other, or
synchronously, with all updates occurring at the same time. The algorithm is
guaranteed to converge for the asynchronous version and, in practice, the
convergence is rapid. For the synchronous version, however, the convergence is
not guaranteed as small oscillations may occur.

\subsubsection{Graph Cuts}

Graph cuts were introduced by \citet{boykov1998markov, boykov2001fast}, who
show that the problem of minimizing the energy function defined in
Equation~\ref{eqn:mrf_energy} reduces to instances of the maximum flow problem
in a graph (or minimum cut, which is related to maximum flow by the min-flow
max-cut theorem). Two algorithms are presented by \citet{boykov2001fast}, one
based on $\alpha$-$\beta$-swap moves and one based on $\alpha$-expansion
moves. These algorithms rapidly compute a local minimum in the sense that no
permitted move produces a labelling with lower energy. The two moves are
defined as follows:
%
\begin{itemize}

\item For a pair of labels $\alpha$ and $\beta$ the $\alpha$-$\beta$-swap move
exchange the labels between an arbitrary set of pixels labeled $\alpha$ and an
arbitrary set labeled $\beta$.

\item For a label $\alpha$ the $\alpha$-expansion move assigns an arbitrary set of
pixels the label $\alpha$.

\end{itemize}
%
The criteria for a local minimum with respect to swap moves or expansion moves
are stronger than moves from ICM and there are much fewer local minima,
especially in high-dimensional spaces. The $\alpha$-$\beta$-swap move was shown
to be applicable to any energy where $V_\mathbf{pq}$ is a semi-metric, and the
$\alpha$-expansion move algorithm to any energy where $V_\mathbf{pq}$ is a
metric. These conditions were later generalized
by~\citet{kolmogorov2004energy}. 

% The $\alpha$-expansion move can be used if for
% all labels $\alpha$, $\beta$, and $\gamma$,
% %
% \begin{equation}
% V_{\mathbf{pq}}(\alpha, \alpha) + V_{\mathbf{pq}}(\beta, \gamma) \leq 
% V_{\mathbf{pq}}(\alpha, \gamma) + V_{\mathbf{pq}}(\beta, \alpha).
% \end{equation}
% %
% Similarly, the $\alpha$-$\beta$-swap move can be used if for all labels $\alpha$
% and $\beta$,
% %
% \begin{equation}
% V_{\mathbf{pq}}(\alpha, \alpha) + V_{\mathbf{pq}}(\beta, \beta) \leq 
% V_{\mathbf{pq}}(\alpha, \beta) + V_{\mathbf{pq}}(\beta, \alpha).
% \end{equation}
% %
% Furthermore, if the function $V_{\mathbf{pq}}$ does not obey these constraints,
% the graph cuts algorithm can still be applied by ``truncating'' some of the terms
% \cite{rother2005digital}. However in this case we are no longer guaranteed to find
% the optimal labeling with respect to expansion or swap moves. This version of the
% algorithm seems to work well when only a small number of terms have to be truncated.

\subsubsection{Max-Product Loopy Belief Propagation}

% Two main variants of loopy belief propagation exist. The sum-product algorithm
% is used to compute marginal probability distributions of each node in the
% graph, while the max-product algorithm is used to find an assignment of labels
% to pixels that minimize the global energy function. For the problem of stereo,
% the max-product version can be applied directly to minimize the energy function
% defined by Equation~\ref{eqn:mrf_energy}. 

Belief propagation is a message passing
algorithm~\cite{pearl2014probabilistic}. When applied to acyclic graphs, it
finds the global minimum of the energy function. The same algorithm can be
applied to general graphs to give an approximate algorithm. This algorithm is
called loopy belief propagation, because the graphs contain cycles or loops.
The convergence of loopy belief propagation is not guaranteed as it may get
trapped in an infinite loop, switching between labellings. The max-product
version of loopy belief propagation is used to find an assignment of labels to
pixels, while the sum-product version is used to compute the
marginal distributions. A detailed description of the loopy belief propagation
algorithm is given by \citet{freeman2000learning} and
\citet{felzenszwalb2006efficient}.

\subsubsection{Semiglobal Matching}

Semiglobal matching aims to minimize the global $2$D energy function by solving
a large number of $1$D minimization problems~\cite{hirschmuller2008stereo}. The
actual energy function used is defined as
%
\begin{multline}
E(D) = \sum_{\mathbf{p}} \biggl( C(\mathbf{p}, D(\mathbf{p}))
+ \sum_{\mathbf{q} \in \mathcal{N}_{\mathbf{p}}} P_1 \cdot 1\{|D(\mathbf{p}) - D(\mathbf{q})| = 1\} \\
+ \sum_{\mathbf{q} \in \mathcal{N}_{\mathbf{p}}} P_2 \cdot 1\{|D(\mathbf{p}) - D(\mathbf{q})| > 1\} \biggr),
\end{multline}
%
where $P_1$ is the penalty when neighboring pixels differ in disparity by one
pixel and $P_2$ is the penalty for all larger differences in disparity.
Semiglobal matching calculates $E(D)$ along $1$D paths from 8 directions
towards each pixel of interest using dynamic programming. The costs of all
paths are summed for each pixel and disparity.

\subsection{Evaluation}


\begin{figure}[p]
\setlength\tabcolsep{2pt}
\begin{tabular}{cc}
\includegraphics[width=80pt]{img/mb2005/art.jpg}      & \includegraphics[width=80pt]{img/dataset/mb_045_disp.png} \\
\includegraphics[width=80pt]{img/mb2005/books.jpg}    & \includegraphics[width=80pt]{img/dataset/mb_046_disp.png} \\
\includegraphics[width=80pt]{img/mb2005/dolls.jpg}    & \includegraphics[width=80pt]{img/dataset/mb_047_disp.png} \\
\includegraphics[width=80pt]{img/mb2005/laundry.jpg}  & \includegraphics[width=80pt]{img/dataset/mb_048_disp.png} \\
\includegraphics[width=80pt]{img/mb2005/moebius.jpg}  & \includegraphics[width=80pt]{img/dataset/mb_049_disp.png} \\
\includegraphics[width=80pt]{img/mb2005/reindeer.jpg} & \includegraphics[width=80pt]{img/dataset/mb_050_disp.png} \\
\end{tabular}
\caption{The \emph{Art}, \emph{Books}, \emph{Dolls}, \emph{Laundry},
\emph{Moebius}, and \emph{Reindeer} stereo pairs from the Middlebury 2005
data set. The left column contains the reference image and the right
column the ground truth disparity map.}

\label{fig:mb2005}
\end{figure}

This section summarizes the results obtained by~\citet{hirschmuller2009evaluation}. 
The validation is performed on six stereo image pairs taken from the Middlebury 2005
data set: \emph{Art}, \emph{Books}, \emph{Dolls}, \emph{Laundry}, \emph{Moebius}, and
\emph{Reindeer}. See Figure~\ref{fig:mb2005} .
The following matching cost functions were considered: 
%
\begin{itemize}
\item Absolute differences (AD)
\item Birchfield and Tomasi (BT)
\item Mean filter followed by BT (Mean/BT)
\item Laplacian of Gaussian followed by BT (LoG/BT)
\item Background subtraction by bilateral filtering followed by BT (BilSub/BT)
\item Zero-mean sum of absolute differences (ZSAD)
\item Normalized cross-correlation (NCC)
\item Zero-mean normalized cross-correlation (ZNCC)
\item Rank filter followed by AD (Rank/AD)
\item Rank filter followed by BT (Rank/BT)
\item Soft rank filter followed by AD (SoftRank/AD)
\item Soft rank filter followed by BT (SoftRank/BT)
\item Census transform (Census)
\item Ordinal measure (Ordinal)
\item Hierarchical mutual information (HMI)
\end{itemize}
%
Some of the matching costs (NCC, ZSAD, ZNCC, and Ordinal) can only be used in
window-based matching. Others have been tested with both AD and BT. Results
for AD and BT are both reported only when they produced significantly different
results.

Since the overall accuracy of a stereo system depends not only on the matching
cost but also on the algorithms that uses the cost, the evaluation has to take
this into account. It is the interaction of the matching cost and the
stereo method that is of interest, because in real-life applications a
matching cost is rarely used in isolation and is usually followed by a stereo
method. The following three stereo algorithms were considered:
%
\begin{itemize}
\item A local, windows-based method (Window)
\item Semiglobal matching (SGM)
\item A global method using graph cuts (GC)
\end{itemize}
%
Each matching cost was evaluated with all three stereo methods, except for
NCC, ZSAD, ZNCC, and Ordinal which can only be used with the local method.

\paragraph{Window}
The matching cost is aggregated over a square $9 \times 9$ window and disparity
is determined using the winner-takes-all strategy. The following
post-processing steps are used: subpixel interpolation, to obtain disparities
with subpixel precision, a left-right consistency check, to detect occlusions
and mismatches, and invalidation of disparity segments smaller than 160 pixels.
The invalidated regions are filled in by propagating background disparity
values from valid nearby regions.

\paragraph{SGM}
The semiglobal matching algorithm is applied and the disparities are determined
using the winner-takes-all strategy. The post-processing steps are similar to
the local stereo method: subpixel enhancement, a left-right consistency check,
and invalidation of disparity segments smaller than 20 pixels. The invalidated
disparities are interpolated as in the local method.

\paragraph{GC}
The graph cuts algorithm is applied and the disparities are, again, determined
using the winner-take-all strategy. Unlike Window and SGM, no post-processing
steps are used.

\begin{figure}[p]
\setlength\tabcolsep{0pt}
\begin{tabular}{cc}
(a) Grayscale images & (b) Color and grayscale images \\
\includegraphics[scale=0.5]{img/mc_gray.pdf} &
\includegraphics[scale=0.5]{img/mc_color.pdf} 
\end{tabular}
\caption{Mean $1$ pixel errors on the Middlebury 2005 stereo data set. (a)
Comparing matching costs on grayscale images. (b) How color information
affects the performance of the matching cost.}

\label{fig:mc_mb2005}
\end{figure}

\vspace{8pt}
Error is calculated by counting the number of predicted disparities that differ
from the ground truth by more than one pixel. Only non-occluded regions of the
image are used to determine the error, because occlusions can not be handled by
the matching step. The results of the experiments are presented in
Figure~\ref{fig:mc_mb2005}.

Many cost functions outperform the sum of absolute differences and
Birchfield-Tomasi. It would seem reasonable to expect absolute difference to
work best when corresponding points have the same brightness. Even
though the images of the Middlebury data set were taken in a studio under
controlled conditions the brightness constancy assumption is still violated. To
summarize the results of Figure~\ref{fig:mc_mb2005}, the performance of the
matching costs can depend on the stereo method used, but Census showed the best
performance with all there stereo methods.

The images were converted to grayscale before being matched by the stereo
algorithm. In many applications, however, color images are available and the
additional information might be useful in matching patches. The most promising
costs were reimplemented for color images by applying them separately on the
red, green, and blue color channels and averaging the results. The results
are shown in Figure~\ref{fig:mc_mb2005} and seem to indicate that using color
has little overall benefit.
