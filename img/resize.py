import os
import subprocess

### KITTI ###
def convert(fname, scale):
    subprocess.check_output('convert -crop 1242x280+0+0 -gravity South -resize {}% {}.png {}_.png'.format(
        scale, fname, fname).split())

datasets = {
    'kitti': [50, 69],
    'kitti2015': [122, 195],
}

for dataset, nums in datasets.items():
    for num in nums:
        convert('{}_{:06d}_10L'.format(dataset, num), scale=22.0)
        convert('{}_{:06d}_10R'.format(dataset, num), scale=22.0)
        convert('{}_{}_gt'.format(dataset, num), scale=22.0)
        for arch in ['census', 'fast', 'slow']:
            convert('{}_{}_{}_pred'.format(dataset, arch, num), scale=32)
            convert('{}_{}_{}_err'.format(dataset, arch, num), scale=32)


### Middlebury ###
def convert(fname, scale):
    subprocess.check_output('convert -resize {}% {}.png {}_.png'.format(
        scale, fname, fname).split())

convert('mb_7L', scale=10)
convert('mb_7R', scale=10)
convert('mb_7_2_gt', scale=20)
for arch in ['census', 'fast', 'slow']:
    convert('mb_{}_7_2_pred'.format(arch), scale=22)
    convert('mb_{}_7_2_err'.format(arch), scale=22)

