The main scientific contributions of this thesis are two new methods for
computing the stereo matching cost. The contributions are itemized in the
following list:

\begin{itemize}


\item \emph{The accurate architecture---a convolutional neural network for
computing the stereo matching cost optimized for accuracy.}

The accurate architecture consists of two convolutional sub-network that
extract feature vectors from small image patches. The feature vectors are
forward-propagated through a multi-layer neural network that computes their
similarity. The accurate architecture is trained on the KITTI and Middlebury
stereo data sets to produce a good matching cost. Several post-processing steps
are applied to improve the quality of the disparity maps. The error rates of
the accurate architecture are 2.43\,\% on KITTI 2012, 3.89\,\% on KITTI 2015,
and 8.29\,\% on the Middlebury data set. At the time of publication (November
2015), these were the lowest error rates on all three data sets. 

The proposed method was orally presented in the Reconstruction Meets
Recognition Challenge workshop at the European Conference on Computer Vision
2014 in Zurich and was published in the Computer Vision and Pattern Recognition
conference 2015~\cite{Zbontar_2015_CVPR}, which took place in Boston. An
improved version of the accurate architecture is described in our Journal of
Machine Learning Research paper in 2016~\cite{zbontar2015stereo}.

\item \emph{The fast architecture---a convolutional neural network for computing
the stereo matching cost optimized for speed.}

The accurate architecture produces precise disparity maps, however its range of
application is limited to those that do not require real-time processing. After
analyzing the network, we discovered that most of the time is spent in the
multi-layer neural network with only a small fraction of time spent in
the convolutional sub-networks. By replacing the multi-layer neural network
with a fast operation---the dot product---we were able to compute the disparity
maps up to 90 times faster. The error rates of the fast architecture are
2.82\,\% on KITTI 2012, 4.62\,\% on KITTI 2015, and 9.69\,\% on the Middlebury
stereo data set. On small 320 $\times$ 240 resolution images with 32 disparity
levels the output of the fast architecture is computed in 30 milliseconds,
which is 60 times faster compared with the 1.8 seconds required by the accurate
architecture.

The fast architecture was first presented in our Journal of Machine Learning
Research paper in 2016~\cite{zbontar2015stereo}.

\item \emph{The source code of both methods was made available under a permissive
free software license.}

The source code of both the fast and the accurate architecture is available
online at \url{https://github.com/jzbontar/mc-cnn} under the permissive BSD
licence. Several groups already use it in their work ~\cite{guney2015displets,
anonymous2016very, anonymous2016adaptive, barron2015fast, anonymous2016stereo}
and outperform our method on the KITTI and Middlebury data sets. At the time of
writing (April 2016), the most accurate stereo methods on both the
KITTI~\cite{guney2015displets} and Middlebury~\cite{anonymous2016adaptive} data
sets use our code to compute the stereo matching cost. Releasing the source
code was an important contribution to the field as it allowed other researchers
to build on our work and push the boundaries of stereo matching.

\end{itemize}

The proposed stereo methods were thoroughly tested and compared with existing
approaches. We evaluated different data augmentation steps, measured the
runtime over many hyperparameter settings, compared the error rate against
established matching cost functions, evaluated the effect of each step of the
post-processing, simulated experiments on smaller data sets, measured the
network's performance on the transfer learning setting by training on one data
set and testing on a different data set, and compared many different
hyperparameter settings.
