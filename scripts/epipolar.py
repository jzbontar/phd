from PIL import Image, ImageDraw
import numpy as np
import cv2


left_ps = np.array([(650, 486), (834, 831), (856, 374), (610, 131), (959, 829), (1007, 535), (1035, 252), (590, 220)], dtype=np.float64)
right_ps = np.array([(549, 509),(645, 865), (746, 318), (511, 176), (807, 870), (989, 457), (1042, 34), (467, 274)], dtype=np.float64)

# Y = []
# for (l1, l2), (r1, r2) in zip(left_ps, right_ps):
#     Y.append([
#         r1 * l1, 
#         r1 * l2, 
#         r1, 
#         r2 * l1, 
#         r2 * l2, 
#         r2, 
#         l1, 
#         l2, 
#         1])
# 
# Y = np.array(Y, dtype=np.float64)
# u, s, v = np.linalg.svd(Y)
# F = v[-1].reshape(3, 3).T
# 
# print(left_ps)
# print(right_ps)

F, mask = cv2.findFundamentalMat(right_ps, left_ps)

left = Image.open('left.jpg')
right = Image.open('right.jpg')
left_draw = ImageDraw.Draw(left)
right_draw = ImageDraw.Draw(right)

R = 12
for i in [0, 1, 2, 3, 7]:
    l = np.concatenate([left_ps[i], [1]])
    r = np.concatenate([right_ps[i], [1]])

    a, b, c = l.dot(F)
    y_0 = -c / b
    y_1800 = (-c - a*1800) / b
    right_draw.line((0, y_0, 1800, y_1800), 'white', 12)
    right_draw.ellipse((r[0] - R, r[1] - R, r[0] + R, r[1] + R), 'red')

    a, b, c = F.dot(r)
    y_0 = -c / b
    y_1800 = (-c - a*1800) / b
    left_draw.line((0, y_0, 1800, y_1800), 'white', 12)
    left_draw.ellipse((l[0] - R, l[1] - R, l[0] + R, l[1] + R), 'red')

left.save('left_e.jpg')
right.save('right_e.jpg')
