import re
import urllib
import pickle
import numpy as np
import scipy.stats

import mb


def get_errs(result):
    s = urllib.urlopen('http://www.cvlibs.net/datasets/kitti/eval_stereo_flow_detail.php?benchmark=stereo&error=3&eval=all&result=' + result).read()
    errs = []
    for res in re.findall('<td class="results">3 pixels</td>(.*?)</tr>', s, re.DOTALL):
        res = re.findall('\d+\.\d+', res)
        assert len(res) == 4
        errs.append(float(res[0]))
    errs = errs[2:]
    assert len(errs) == 20
    return errs

test_on_methods = map(str.strip, 'MC-CNN-acrt, Displets, MC-CNN, PRSM, SPS-StFl, VC-SF, Deep Embed, JSOSM, OSF, CoR'.split(','))

s = urllib.urlopen('http://www.cvlibs.net/datasets/kitti/eval_stereo_flow.php?benchmark=stereo').read()
ms = re.findall(r'<a href="eval_stereo_flow_detail.php\?benchmark=stereo&error=3&eval=all&result=(.*?)">(.*?)</a>', s)
ms = {k.strip(): v for v, k in ms}
scores = []
for m in test_on_methods:
    scores.append(get_errs(ms[m]))
scores = np.array(scores)
print(scores)

ranks = np.empty_like(scores)
for j in range(scores.shape[1]):
    ranks[:,j] = scipy.stats.rankdata(scores[:,j])
print('ranks')
print(ranks)

rs = np.mean(ranks, axis=1)
print('avg ranks')
print(rs)

mb.janez(ranks.shape[1], ranks.shape[0], rs)
for rank, method in zip(rs, test_on_methods):
    print('{} {:.2f}'.format(method, rank))

# import Orange
# 
# cd = Orange.evaluation.scoring.compute_CD(rs, ranks.shape[1], alpha='0.05')
# print(cd)
# Orange.evaluation.scoring.graph_ranks("k12.png", rs, test_on_methods, cd=cd, width=6, textspace=1.5)
