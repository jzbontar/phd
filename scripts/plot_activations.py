import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": ["Adobe Garamond Pro"],
    "font.sans-serif": ["Adobe Sans Pro"],
    "font.monospace": ["Source Code Pro"],
}
mpl.rcParams.update(pgf_with_rc_fonts)


import pylab as plt
import numpy as np

x = np.linspace(-4, 4, 101)

plt.axis((-4, 4, -2, 3))
plt.plot(x, 1 / (1 + np.exp(-x)), 'k-', label='logistic sigmoid activation')
plt.plot(x, np.tanh(x), 'k--', label='tahn activation')
plt.plot(x, np.maximum(0, x), 'k:', label='rectified linear unit')

plt.legend(loc=2)
plt.savefig('../img/activations.pdf')
