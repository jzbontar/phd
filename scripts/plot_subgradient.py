import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": ["Adobe Garamond Pro"],
    "font.sans-serif": ["Source Sans Pro"],
    "font.monospace": ["Source Code Pro"],
}
mpl.rcParams.update(pgf_with_rc_fonts)


import pylab as plt
import numpy as np

x = np.linspace(-4, 4, 101)

plt.figure(figsize=(4, 3))
# plt.grid()
plt.axis((-4, 4, -2, 3))

plt.plot(x, 0.4 * x, 'r')
plt.plot(x, 0.6 * x, 'r')
plt.plot(x, np.maximum(0, x), 'k')

plt.savefig('../img/subgradient.pdf')
#plt.show()
