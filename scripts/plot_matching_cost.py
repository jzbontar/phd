import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": ["Adobe Garamond Pro"],
    "font.sans-serif": ["Source Sans Pro"],
    "font.monospace": ["Source Code Pro"],
}
mpl.rcParams.update(pgf_with_rc_fonts)

import pylab as plt
import numpy as np

def f(ys):
    return [(233 - y) / 233. * 30. for y in ys]

plt.figure(figsize=(4, 10))
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]

color = np.array(tableau20[0]) / 255.

y = f([58, 51, 82, 114, 127, 105, 112, 110, 127, 132, 127, 125, 146, 119, 100])
x = ['SAD', 'BT', 'Mean/BT', 'LoG/BT', 'BilSub/BT', 'NCC', 'ZSAD', 'ZNCC', 'Rank/SAD', 'Rank/BT',
'SoftRank/SAD', 'SoftRank/BT', 'Census', 'Ordinal', 'HMI']

ax = plt.subplot(311)
plt.title('Window')
ax.xaxis.grid(True)
plt.ylim([-1, len(x)])
x_pos = np.arange(len(x))
plt.barh(x_pos, y, color=color, align='center', tick_label=x)
plt.xlabel('Errors of non-ocluded pixels (\%)')

y = f([104, 104, 106, 150, 165, 118, 155, 140, 157, 178, 128])
x = ['AD', 'BT', 'Mean/BT', 'LoG/BT', 'BilSub/BT', 'Rank/AD', 'Rank/BT', 'SoftRank/AD',
'SoftRank/BT', 'Census', 'HMI']

ax = plt.subplot(312)
plt.title('SGM')
ax.xaxis.grid(True)
plt.ylim([-1, len(x)])
x_pos = np.arange(len(x))
plt.barh(x_pos, y, color=color, align='center', tick_label=x)
plt.xlabel('Errors of non-ocluded pixels (\%)')

y = f([92, 92, 71, 116, 142, 134, 139, 128, 130, 158, 111])
x = ['AD', 'BT', 'Mean/BT', 'LoG/BT', 'BilSub/BT', 'Rank/AD', 'Rank/BT', 'SoftRank/AD',
'SoftRank/BT', 'Census', 'HMI']

ax = plt.subplot(313)
plt.title('GC')
ax.xaxis.grid(True)
plt.ylim([-1, len(x)])
x_pos = np.arange(len(x))
plt.barh(x_pos, y, color=color, align='center', tick_label=x)
plt.xlabel('Errors of non-ocluded pixels (\%)')

plt.tight_layout()
plt.savefig('../img/mc_gray.pdf')
