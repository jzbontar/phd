import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": ["Adobe Garamond Pro"],
    "font.sans-serif": ["Source Sans Pro"],
    "font.monospace": ["Source Code Pro"],
}
mpl.rcParams.update(pgf_with_rc_fonts)

import pylab as plt
import numpy as np

def f(ys):
    return [(242 - y) / 242. * 25. for y in ys]

plt.figure(figsize=(4, 10))
plt.rc('text', usetex=True)
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),    
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),    
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),    
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),    
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]


color_gray = np.array(tableau20[0]) / 255.
color_rgb = np.array(tableau20[1]) / 255.

width = 0.3

x = ['BT', 'BilSub/BT', 'ZNCC', 'Census', 'HMI']
x_pos = np.arange(len(x)) + 0.15
y_gray = f([15, 111, 90, 134, 74])
y_color = f([35, 128, 86, 137, 85])

ax = plt.subplot(311)
ax.xaxis.grid(True)
plt.title('Window')
plt.barh(x_pos + width, y_gray, width, color=color_gray, label='gray')
plt.barh(x_pos, y_color, width, color=color_rgb, label='color')
plt.yticks(x_pos + width, x)
plt.xlabel('Errors of non-ocluded pixels (\%)')
plt.xlim([0, 35])
plt.legend(loc=7)


x = ['BT', 'BilSub/BT', 'Census', 'HMI']
x_pos = np.arange(len(x)) + 0.15
y_gray = f([73, 145, 164, 105])
y_color = f([89, 162, 165, 128])

ax = plt.subplot(312)
ax.xaxis.grid(True)
plt.title('SGM')
plt.barh(x_pos + width, y_gray, width, color=color_gray, label='gray')
plt.barh(x_pos, y_color, width, color=color_rgb, label='color')
plt.yticks(x_pos + width, x)
plt.xlabel('Errors of non-ocluded pixels (\%)')
plt.xlim([0, 20])
plt.legend(loc=7)


x = ['BT', 'BilSub/BT', 'Census', 'HMI']
x_pos = np.arange(len(x)) + 0.15
y_gray = f([59, 119, 138, 88])
y_color = f([70, 131, 140, 100])

ax = plt.subplot(313)
ax.xaxis.grid(True)
plt.title('GC')
plt.barh(x_pos + width, y_gray, width, color=color_gray, label='gray')
plt.barh(x_pos, y_color, width, color=color_rgb, label='color')
plt.yticks(x_pos + width, x)
plt.xlabel('Errors of non-ocluded pixels (\%)')
plt.xlim([0, 25])
plt.legend(loc=7)


plt.tight_layout()
plt.savefig('../img/mc_color.pdf')
