import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": ["Adobe Garamond Pro"],
    "font.sans-serif": ["Source Sans Pro"],
    "font.monospace": ["Source Code Pro"],
}
mpl.rcParams.update(pgf_with_rc_fonts)

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.linspace(-3, 3, 41)
Y = np.linspace(-3, 3, 41)
X, Y = np.meshgrid(X, Y)

Z = (0.5 - np.tanh(X * np.tanh(Y * 0.5)))**2

surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet, linewidth=1)
ax.view_init(elev=25, azim=30)

ax.set_xlabel('$w_0$')
ax.set_ylabel('$w_1$')
ax.set_zlabel('$E(w_0, w_1)$')

# plt.show()
plt.savefig('../img/error_surface.pdf')
