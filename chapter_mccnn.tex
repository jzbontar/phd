In this section we describe a new algorithm for computing the stereo matching
cost and show that it outperforms all previously published approaches on the
KITTI 2012, KITTI 2015, and Middlebury stereo data sets. This is the main
contribution of the dissertation. 

To learn a good matching cost, we propose training a convolutional neural
network on pairs of small image patches where the true disparity is known (for
example, obtained by LIDAR or structured light). The output of the network is
used to initialize the matching cost. We proceed with a number of
post-processing steps that are not novel, but are necessary to achieve good
results. Matching costs are combined between neighboring pixels with similar
image intensities using cross-based cost aggregation. Smoothness constraints
are enforced by semiglobal matching, and a left-right consistency check is used
to detect and eliminate errors in occluded regions. We perform subpixel
enhancement and apply a median filter and a bilateral filter to obtain the
final disparity map. 

\section{Matching Cost}

We approach the problem of computing the matching cost by using a supervised
learning approach. In this section we describe how the data set is constructed,
which models are used to learn the mapping from image patches to matching
costs, and how we trained them. We use \texttt{typewriter} font for the names
of hyperparameters.

\subsection{Constructing the Data Set}
\label{sec:dataset}

We use ground truth disparity maps from either the KITTI or Middlebury stereo
data sets to construct a binary classification data set. At each image position
where the true disparity is known we extract one negative and one positive
training example. This ensures that the data set contains an equal number of
positive and negative examples. A positive example is a pair of patches, one
from the left and one from the right image, whose center pixels are the
images of the same 3D point, while a negative example is a pair of patches
where this is not the case. The following section describes the data set
construction step in detail.

Let
$
<P_{n \times n}^L(\mathbf{p}), P_{n \times n}^R
(\mathbf{q})>
$
%
denote a pair of patches, where $P_{n \times n}^L(\mathbf{p})$ is an
$n \times n$ patch from the left image centered at position $\mathbf{p} = (x, y)$,
$P_{n \times n}^R(\mathbf{q})$ is an $n \times n$ patch from the right image
centered at position $\mathbf{q}$, and $d$ denotes the correct disparity at position
$\mathbf{p}$. A negative example is obtained by setting the center of the right
patch to
%
\begin{equation*}
\mathbf{q} = (x - d + o_{\text{neg}}, y),
\end{equation*}
%
where $o_{\text{neg}}$ is chosen from either the interval
\([\texttt{dataset\_neg\_low}, \texttt{dataset\_neg\_high}]\) or, its origin
reflected counterpart, \([-\texttt{dataset\_neg\_high},
-\texttt{dataset\_neg\_low}]\). The random offset $o_{\text{neg}}$ ensures
that the resulting image patches are not centered around the same 3D point. 

A positive example is derived by setting
%
\begin{equation*}
\mathbf{q} = (x - d + o_{\text{pos}}, y),
\end{equation*}
%
where $o_{\text{pos}}$ is chosen randomly from the interval
\([-\texttt{dataset\_pos}, \texttt{dataset\_pos}]\).  The reason for including
$o_{\text{pos}}$, instead of setting it to zero, has to do with the stereo
method used later on. In particular, we found that cross-based cost aggregation
performs better when the network assigns low matching costs to good matches as
well as near matches. In our experiments, the hyperparameter \texttt{dataset\_pos}
was never larger than one pixel.

\subsection{Network Architectures}

We describe two network architectures for learning a similarity measure on
image patches. The first architecture is faster than the second, but produces
disparity maps that are slightly less accurate. In both cases, the input to the
network is a pair of small image patches and the output is a measure of
similarity between them. Both architectures contain a trainable feature
extractor that represents each image patch with a feature vector. The
similarity between patches is measured on the feature vectors instead of the
raw image intensity values. The fast architecture uses a fixed similarity
measure to compare the two feature vectors, while the accurate architecture
attempts to learn a good similarity measure on feature vectors. 

\subsubsection{Fast Architecture}

The first architecture is a siamese network, that is, two shared-weight
sub-networks joined at the head \citep{bromley1993signature}.  The sub-networks
are composed of a number of convolutional layers with rectified linear units
following all but the last layer. Both sub-networks output a vector capturing
the properties of the input patch. The resulting two vectors are compared using
the cosine similarity measure to produce the final output of the network.
Figure~\ref{fig:architecture_fast} provides an overview of the architecture. 

\begin{figure}[tb]
\begin{center}
\includegraphics{img/architecture_fast.pdf}
\end{center}
\caption{The fast architecture is a siamese network. The two sub-networks
consist of a number of convolutional layers followed by rectified linear units
(abbreviated ``ReLU''). The similarity score is obtained by extracting a vector
from each of the two input patches and computing the cosine similarity between
them. On this diagram, as in our implementation, the computation of the cosine
similarity is split in two steps: normalization and dot product. This way, the
normalization needs to be performed only once per position (see
Section~\ref{sec:matching_cost}).}

\label{fig:architecture_fast}
\end{figure}

The network is trained by minimizing a hinge loss. The loss is computed by
considering pairs of examples centered around the same image position where one
example belongs to the positive and one to the negative class. Let \( s_{+} \)
be the output of the network for the positive example, \( s_{-} \) be the
output of the network for the negative example, and let \(m\), the margin, be a
positive real number. The hinge loss for that pair of examples is defined as
$\max(0, m + s_{-} - s_{+})$. The loss is zero when the similarity of the
positive example is greater than the similarity of the negative example by at
least the margin \(m\). The margin was treated as a hyperparameter and was set
to 0.2 after performing a grid search over the range from 0 to 1.

Since the hinge loss function depends on both the score of the positive as well
as the score of the negative example, we have to ensure that both examples
appear in the same mini-batch during training. If the size of the mini-batch is
$n$, this is achieved by selecting $n/2$ random training images and positions;
one positive and one negative example is extracted from each position as
described in Section~\ref{sec:dataset}. Once the outputs of all examples
of the mini-batch have been computed, the gradients are obtained by the
backpropagation algorithm and the weights are updated accordingly.

The hyperparameters of this architecture are 
the number of convolutional layers in each sub-network (\texttt{num\_conv\_layers}), 
the size of the convolution kernels (\texttt{conv\_kernel\_size}), 
the number of feature maps in each layer (\texttt{num\_conv\_feature\_maps}), 
and the size of the input patch (\texttt{input\_patch\_size}).

\subsubsection{Accurate Architecture}

The second architecture is derived from the first by replacing the cosine
similarity with a number of fully-connected layers (see
Figure~\ref{fig:architecture_accurate}). This architectural change increased
the running time, but decreased the error rate. The two sub-networks comprise a
number of convolutional layers, with a rectified linear unit following each
layer. The resulting two vectors are concatenated and forward-propagated
through a number of fully-connected layers followed by rectified linear units. The
last fully-connected layer produces a single number which, after being
transformed with the sigmoid nonlinearity, is interpreted as the similarity score
between the input patches.

\begin{figure}[tb] 
\begin{center}
\includegraphics{img/architecture_accurate.pdf} 
\caption{The accurate architecture begins with two convolutional feature
extractors. The extracted feature vectors are then concatenated and compared by
a number of fully-connected layers. The inputs are two image patches and the
output is a single real number between 0 and 1, which we interpret as a measure
of similarity between the input images.}

\label{fig:architecture_accurate}
\end{center}
\end{figure}

We use the binary cross-entropy loss for training.  Let $s$ denote the output of
the network for one training example and $t$ denote the class of that
training example; $t=1$ if the example belongs to the positive class and $t=0$
if the example belongs to the negative class. The binary cross-entropy loss for
that example is defined as $t\log(s) + (1 - t)\log(1 - s)$.

The decision to have two different loss functions, one for each architecture,
was based on empirical evidence. We would have preferred to use the same loss
function for both architectures, but experiments showed that the binary
cross-entropy loss performed better than the hinge loss on the accurate
architecture. On the other hand, since the last step of the fast architecture
is the cosine similarity computation, a cross-entropy loss was not directly
applicable. We observed that other researches used similar loss functions for
siamese architectures (\citet{chopra2005learning, hadsell2006dimensionality,
weston2011wsabie}) and typically used a cross-entropy loss when the network
resembled our accurate architecture.


The hyperparameters for the accurate architecture are
the number of convolutional layers in each sub-network (\texttt{num\_conv\_layers}), 
the number of feature maps in each layer (\texttt{num\_conv\_feature\_maps}), 
the size of the convolution kernels (\texttt{conv\_kernel\_size}), 
the size of the input patch (\texttt{input\_patch\_size}), 
the number of units in each fully-connected layer (\texttt{num\_fc\_units}),
and the number of fully-connected layers (\texttt{num\_fc\_layers}). 

\subsubsection{Designing the Network Architecture}

When developing algorithms for computing the stereo matching cost we were
guided by the following two questions:

\begin{itemize}

\item \emph{Should the similarity measure be computed on raw image intensity
values or on feature vectors extracted from patches?}

A possible advantage of computing the similarity on feature vectors is that the
representation can learn to be insensitive to some transformations, for example
small changes in brightness, viewpoint, and vertical disparity; while being
sensitive to others, for example, small changes in horizontal disparity.

\item \emph{Can we design an architecture with a fast matching step?}

The process of computing the similarity of image patches decomposes into two
steps: the feature extraction step and the comparison step. In the accurate
architecture, for example, feature vectors are extracted by the convolutional
sub-networks and compared by the fully-connected neural network. The feature
extraction step is run once, but the comparison step needs to be run once for
each disparity under consideration. It is, therefore, important that the
operation performed in the comparison step are efficient. This was the
motivation for the fast architecture, where the dot product is the only
operation performed in the comparison step.

\end{itemize}

We were driven by empirical evidence when designing the architecture of the
network. We tried several matching cost network architectures, most of which
aren't described in this thesis. When experimenting with a new architecture, we
computed its validation error and compared it to our baseline model. If the
validation error, which was obtained using a single 80/20 split of the data
set, looked promising, the model was explored further, otherwise a new
architecture was tested. 

We experimented with the following approaches: 
%
\begin{itemize}

\item Adding max or average pooling and subsampling. 

\item Adding dropout~\cite{srivastava2014dropout}. 

\item Using a hierarchical approach, that is, predicting the disparity of downsampled
versions of the images and combining the results. 

\item Posing the stereo problem as a regression problem. Instead of performing
binary classification, we tried predicting the amount of shift that would make
the two patches well registered.

\item Untying the weights of the sub-networks, which would make the
feature extractor for the left patch compute a different function than the
feature extractor for the right patch. 

\item Back-propagating through the linear
search for the best correspondence. 

\item Back-propagating through semiglobal
matching. 

\item We experimented with two alternatives to concatenating the feature vectors in
the accurate architecture. We tried adding them,
%
\begin{equation}
f_i = f^L_i + f^R_i,
\end{equation}
%
where $f^L$ and $f^R$ denote the feature vectors extracted from the left and
right patches. We also experimented with using a bilinear projection,
%
\begin{equation} 
f_i = b_i + \sum_{j,k} f^L_j W_{ijk} f^R_k,
\end{equation}
%
where $\mathbf{W}$ is a tensor of weights and $\mathbf{b}$ is a vector of
biases. A bilinear projection was used in the paper describing deep tensor
neural networks by \citet{yu2013deep}.

% vs 0.0261
% 0.026799437435041 main.sh.o8023289 -t 1 -t_fm 64 -t_nh2 64
% 0.027061646927078 main.sh.o8014748 -t 1 -t_fm 48 -t_nh2 48
% 0.027205516027764 main.sh.o8014739 -t 1 -t_fm 32 -t_nh2 64
% 0.027541032828526 main.sh.o8014730 -t 1 -t_fm 32 -t_nh2 32
% 0.028058804748499 main.sh.o8014762 -t -1 -t_fm 32 -t_nh2 32

\item Concatenating the two patches before presenting them to the network, that
is, an extreme version of the accurate architecture without the sub-networks
but with a single column of convolutional layers followed by fully-connected
layers. 

\item As is evident from Table~\ref{tbl:hyperparameters}, we also experimented
with many different hyperparameters of the two chosen architectures. For
example, the number of convolutional layer, the number of feature maps, and the
number of fully-connected layers.

\end{itemize}



\subsection{Computing the Matching Cost}
\label{sec:matching_cost}

The output of the network is used to initialize the matching cost:
%
\begin{equation*}
C_{\text{CNN}}(\mathbf{p}, d) = -s(<P^L(\mathbf{p}), P^R(\mathbf{p}-\mathbf{d})>),
\end{equation*}
%
where $s(<P^L(\mathbf{p}), P^R(\mathbf{p}-\mathbf{d})>)$ is the
output of the network when run on input patches $P^L(\mathbf{p})$ and
$P^R(\mathbf{p}-\mathbf{d})$. The minus sign converts the similarity score to a
matching cost. 

To compute the entire matching cost tensor \(
C_{\text{CNN}}(\mathbf{p}, d) \) we would, naively, have to perform the forward
pass for each image location and each disparity under consideration. The
following three implementation details kept the running time manageable:
%
\begin{itemize}
\item The outputs of the two sub-networks need to be computed only once per
location, and do not need to be recomputed for every disparity under
consideration.

\item The output of the two sub-networks can be computed for all pixels in a
single forward pass by propagating full-resolution images, instead of small
image patches. Performing a single forward pass on the entire $w \times h$
image is faster than performing $w \cdot h$ forward passes on small patches
because many intermediate results can be reused.

To make this step more concrete and easier to understand, let us calculate the
sizes of the input and output tensors of the first convolutional layer. Assume
that the batch size is 128, the number of feature maps is 112, the size of the
kernel is $3 \times 3$, the patch size is $9 \times 9$, and the size of the
test image is $800 \times 600$. During training the input to the first
convolutional layer is $128 \times 1 \times 9 \times 9$ and the output is $128
\times 112 \times 7 \times 7$. The spatial resolution decreases from $9 \times
9$ to $7 \times 7$ because of the boundary effects of convolution. Since the
output of the convolutional layer is computed by replicating the 112 feature
detectors at each position in the image, we can apply the same convolutional
layer to inputs of different spatial dimensions. At test time the input to the first
convolutional layer is $2 \times 1 \times 800 \times 600$ and the output is $2
\times 112 \times 800 \times 600$. The first dimension is 2, because we forward
propagate the left and right image. Note that in order to preserve the spatial
resolution of $800 \times 600$ we add zero padding at test time.

\item The output of the fully-connected layers in the accurate architecture can
also be computed in a single forward pass. This is done by replacing each
fully-connected layer with a convolutional layer with \(1 \times 1\) kernels.
We still need to perform the forward pass for each disparity under
consideration; the maximum disparity $d$ is 228 for the KITTI data set and 400
for the Middlebury data set. As a result, the fully-connected part of the
network needs to be run \(d\) times, and is a bottleneck for the accurate
architecture.

\end{itemize}
%
To compute the matching cost of a pair of images, we run the sub-networks once
on each image and run the fully-connected layers $d$ times, where $d$ is the
maximum disparity under consideration. This insight was important in designing
the architecture of the network. We could have chosen an architecture where the
two images are concatenated before being presented to the network, but that
would imply a large cost at runtime because the whole network would need to be run
$d$ times. This insight also led us to the fast architecture, where the only
layer that is run $d$ times is the dot product of the feature vectors.

\section{Stereo Method}
\label{sec:stereo_method}

The raw outputs of the convolutional neural network are not enough to produce
accurate disparity maps, with errors particularly apparent in low-texture
regions and occluded areas. The quality of the disparity maps can be improved
by applying a series of post-processing steps referred to as the stereo method.
The stereo method we used was influenced by \citet{mei2011building} and
comprises cross-based cost aggregation, semiglobal matching, a left-right
consistency check, subpixel enhancement, a median, and a bilateral filter.

\subsection{Cross-based Cost Aggregation}

Information from neighboring pixels can be combined by averaging the matching
cost over a fixed window. This approach fails near depth discontinuities,
where the assumption of constant depth within a window is violated. We might
prefer a method that adaptively selects the neighborhood for each pixel, so
that support is collected only from pixels of the same physical object. In
cross-based cost aggregation \citep{zhang2009cross} we build a local
neighborhood around each location comprising pixels with similar image
intensity values with the hope that these pixels belong to the same object.

The method begins by constructing an upright cross at each position; this cross
is used to define the local support region. The left arm $\mathbf{p}_l$ at
position $\mathbf{p}$ extends left as long as the following two conditions
hold: 

\begin{itemize}

\item $|I(\mathbf{p}) - I(\mathbf{p}_l)| < \texttt{cbca\_intensity}$; the image
intensities at positions $\mathbf{p}$ and $\mathbf{p}_l$ should be similar,
their difference should be less than \texttt{cbca\_intensity}.

\item $\|\mathbf{p} - \mathbf{p}_l\| < \texttt{cbca\_distance}$; the horizontal
distance (or vertical distance in case of top and bottom arms) between positions
$\mathbf{p}$ and $\mathbf{p}_l$ is less than \texttt{cbca\_distance} pixels.

\end{itemize}
The right, bottom, and top arms are constructed analogously. Once the four arms
are known, we can compute the support region $U(\mathbf{p})$ as the union of
horizontal arms of all positions $\mathbf{q}$ laying on $\mathbf{p}$'s
vertical arm (see Figure \ref{fig:cross}).

\begin{figure}[tb]
\begin{center}
\includegraphics[scale=1.0]{img/cross}
\end{center}
\caption{The support region for position $\mathbf{p}$ is the union of
horizontal arms of all positions $\mathbf{q}$ on $\mathbf{p}$'s vertical arm.}
\label{fig:cross}
\end{figure}

\citet{zhang2009cross} suggest that aggregation should consider the support
regions of both images in a stereo pair. Let $U^L$ and $U^R$ denote the support
regions in the left and right image. We define the combined support region
$U_d$ as
%
\begin{equation*}
U_d(\mathbf{p}) = \{\mathbf{q} | \mathbf{q} \in U^L(\mathbf{p}), \mathbf{q}-\mathbf{d}
\in U^R(\mathbf{p}-\mathbf{d})\}.
\end{equation*}

The matching cost is averaged over the combined support region:
%
\begin{align*}
C^0_{\text{CBCA}}(\mathbf{p}, d) &= C_{\text{CNN}}(\mathbf{p}, d), \\
C^i_{\text{CBCA}}(\mathbf{p}, d) &= \frac{1}{|U_d(\mathbf{p})|}
\sum_{\mathbf{q} \in U_d(\mathbf{p})} C^{i-1}_{\text{CBCA}}(\mathbf{q}, d),
\end{align*}
%
where $i$ is the iteration number. We repeat the averaging a number of times.
Since the support regions are overlapping, the results can change at each
iteration. We skip cross-based cost aggregation in the fast architecture
because it is not crucial for achieving a low error rate and because it is
relatively expensive to compute.

\subsection{Semiglobal Matching}

We refine the matching cost by enforcing smoothness constraints on the
disparity image. Following \citet{hirschmuller2008stereo}, we define an energy
function $E(D)$ that depends on the disparity image $D$:
%
\begin{multline*} 
E(D) = \sum_{\mathbf{p}} \biggl( C^4_{\text{CBCA}}(\mathbf{p}, D(\mathbf{p})) \\
+ \sum_{\mathbf{q} \in N_{\mathbf{p}}} P_1 \cdot 1\{|D(\mathbf{p}) - D(\mathbf{q})| = 1\} \\
+ \sum_{\mathbf{q} \in N_{\mathbf{p}}} P_2 \cdot 1\{|D(\mathbf{p}) - D(\mathbf{q})| > 1\} \biggr), 
\end{multline*}
%
where $1\{\cdot\}$ denotes the indicator function. The first term penalizes
disparities with high matching costs. The second term adds a penalty $P_1$ when
the disparity of neighboring pixels differ by one. The third term adds a
larger penalty $P_2$ when the neighboring disparities differ by more than one.

Rather than minimizing $E(D)$ in all directions simultaneously, we could
perform the minimization in a single direction with dynamic programming. This
solution would introduce unwanted streaking effects, since there would be no
incentive to make the disparity image smooth in the directions we are not
optimizing over.  In semiglobal matching we minimize the energy in a single
direction, repeat for several directions, and average to obtain the final
result.  Although \citet{hirschmuller2008stereo} suggested choosing sixteen
directions, we only optimized along the two horizontal and the two vertical
directions; adding the diagonal directions did not improve the accuracy of our
system.  To minimize $E(D)$ in direction $\mathbf{r}$, we define a matching
cost $C_{\mathbf{r}}(\mathbf{p}, d)$ with the following recurrence relation:
%
\begin{multline*} 
C_{\mathbf{r}}(\mathbf{p}, d) = C^4_{\text{CBCA}}(\mathbf{p}, d) - \min_k C_r(\mathbf{p} - \mathbf{r}, k) \\
+ \min\biggl\{ C_r(\mathbf{p} - \mathbf{r}, d),
C_r(\mathbf{p} - \mathbf{r}, d - 1) + P_1, \\
C_r(\mathbf{p} - \mathbf{r}, d + 1) + P_1,
\min_k C_{\mathbf{r}}(\mathbf{p} - \mathbf{r}, k) + P_2 \biggr\}. 
\end{multline*}
%
The second term is subtracted to prevent values of $C_\mathbf{r}(\mathbf{p},
d)$ from growing too large and does not affect the optimal disparity map. 

The penalty parameters $P_1$ and $P_2$ are set according to the image gradient
so that jumps in disparity coincide with edges in the image. Let $D_1 =
|I^L(\mathbf{p}) - I^L(\mathbf{p} - \mathbf{r})|$ and $D_2 = |I^R(\mathbf{p}-\mathbf{d})
- I^R(\mathbf{p}-\mathbf{d} - \mathbf{r})|$ be the difference in image intensity between
two neighboring positions in the direction we are optimizing over. We set
$P_1$ and $P_2$ according to the following rules:
%
\[ \begin{array}{lll} 
P_1 = \texttt{sgm\_P1}, &P_2 = \texttt{sgm\_P2} & 
\text{if $D_1 < \texttt{sgm\_D}, D_2 < \texttt{sgm\_D}$}; \\ 
P_1 = \texttt{sgm\_P1} / \texttt{sgm\_Q2}, &P_2 = \texttt{sgm\_P2} / \texttt{sgm\_Q2} & 
\text{if $D_1 \geq \texttt{sgm\_D}, D_2 \geq \texttt{sgm\_D}$}; \\ 
P_1 = \texttt{sgm\_P1} / \texttt{sgm\_Q1}, &P_2 = \texttt{sgm\_P2} / \texttt{sgm\_Q1} & 
\text{otherwise.} \\ 
\end{array} \]
%
The hyperparameters \texttt{sgm\_P1} and \texttt{sgm\_P2} set a base penalty
for discontinuities in the disparity map. The base penalty is reduced by a
factor of \texttt{sgm\_Q1} if one of $D_1$ or $D_2$ indicate a strong image
gradient or by a larger factor of \texttt{sgm\_Q2} if both $D_1$ and $D_2$
indicate a strong image gradient. The value of $P_1$ is further reduced by a
factor of \texttt{sgm\_V} when considering the two vertical directions; in the
ground truth, small changes in disparity are much more frequent in the vertical
directions than in the horizontal directions and should be penalised less. 

The final cost $C_\text{SGM}(\mathbf{p}, d)$ is computed by taking the
average across all four directions:

\begin{equation*} 
C_\text{SGM}(\mathbf{p}, d) = \frac{1}{4} \sum_{\mathbf{r}}
C_{\mathbf{r}}(\mathbf{p}, d). 
\end{equation*}

After semiglobal matching we repeat cross-based cost aggregation, as described
in the previous section. Hyperparameters \texttt{cbca\_num\_iterations\_1}
and \texttt{cbca\_num\_iterations\_2} determine the number of cross-based
cost aggregation iterations before and after semiglobal matching.


\subsection{Computing the Disparity Image}

The disparity image $D(\mathbf{p})$ is computed by the winner-take-all
strategy, that is, by finding the disparity $d$ that minimizes $C(\mathbf{p},
d)$:
%
\begin{equation*}
D(\mathbf{p}) = \arg\!\min_d C(\mathbf{p}, d).
\end{equation*}

\subsubsection{Interpolation}

The interpolation steps attempt to resolve conflicts between the disparity
map predicted for the left image and the disparity map predicted for the right
image. Let $D^L$ denote the disparity map obtained by treating the left image
as the reference image---this was the case so far, that is, $D^L(\mathbf{p}) =
D(\mathbf{p})$---and let $D^R$ denote the disparity map obtained by treating
the right image as the reference image. $D^L$ and $D^R$ sometimes disagree on
what the correct disparity at a particular position should be. We detect these
conflicts by performing a left-right consistency check. We label each
position $\mathbf{p}$ by applying the following rules in turn:
%
\[ \begin{array}{ll} 
correct & \text{if $|d - D^R(\mathbf{p}-\mathbf{d})| \leq 1$ for $d = D^L(\mathbf{p})$}, \\ 
mismatch & \text{if $|d - D^R(\mathbf{p}-\mathbf{d})| \leq 1$ for any other $d$}, \\ 
occlusion & \text{otherwise}. \\ 
\end{array} \]
%
For positions marked as \emph{occlusion}, we want the new disparity value to
come from the background. We interpolate by moving left until we find a
position labeled \emph{correct} and use its value. For positions marked as
\emph{mismatch}, we find the nearest \emph{correct} pixels in 16 different
directions and use the median of their disparities for interpolation. 
We refer to the interpolated disparity map as $D_{\text{INT}}$.

\subsubsection{Subpixel Enhancement}

Subpixel enhancement provides an easy way to increase the resolution of a
stereo algorithm. We fit a quadratic curve through the neighboring costs to
obtain a new disparity image:
%
\begin{equation*}
D_{\text{SE}}(\mathbf{p}) = d - \frac {C_+ - C_-} {2 (C_+ - 2 C + C_-)},
\end{equation*}
%
where
$d = D_{\text{INT}}(\mathbf{p})$,
$C_- = C_{\text{SGM}}(\mathbf{p}, d - 1)$,
$C   = C_{\text{SGM}}(\mathbf{p}, d    )$, and
$C_+ = C_{\text{SGM}}(\mathbf{p}, d + 1)$.

\subsubsection{Refinement}

The final steps of the stereo method consist of a $5 \times 5$ median filter and the
following bilateral filter:
%
\begin{equation*} 
D_{\text{BF}}(\mathbf{p}) = \frac{1}{W(\mathbf{p})}
\sum_{\mathbf{q} \in N_\mathbf{p}} D_{\text{SE}}(\mathbf{q}) \cdot
g(\|\mathbf{p} - \mathbf{q}\|) \cdot 1\{|I^L(\mathbf{p}) - I^L(\mathbf{q})|
< \texttt{blur\_threshold}\},
\end{equation*}
%
where $g(x)$ is the probability density function of a zero mean normal
distribution with standard deviation \texttt{blur\_sigma} and $W(\mathbf{p})$ is the
normalizing constant:
%
\begin{equation*}
W(\mathbf{p}) = \sum_{\mathbf{q} \in N_\mathbf{p}} g(\|\mathbf{p} -
\mathbf{q}\|) \cdot 1\{|I^L(\mathbf{p}) - I^L(\mathbf{q})| <
\texttt{blur\_threshold}\}.
\end{equation*}
%
The role of the bilateral filter is to smooth the disparity map without
blurring the edges. $D_{\text{BF}}$ is the final output of our stereo method.
