Consider the following problem: given two images taken by cameras at
different horizontal positions, we wish to compute the disparity $d$ for each
pixel in the left image. Disparity refers to the difference in horizontal
location of an object in the left and right image---an object at position $(x,
y)$ in the left image appears at position $(x - d, y)$ in the right image. If
we know the disparity of an object we can compute its depth $z$ using the
following relation:
%
\begin{equation}
z = \frac{f B}{d},
\label{eqn:disparity}
\end{equation}
%
where $f$ is the focal length of the camera, $B$ is the distance between the
camera centers. Figure~\ref{fig:input_output} depicts the input to and the
output from a dense two-frame stereo method.

\begin{figure*}[t]
\begin{center}
\includegraphics[scale=0.6]{img/input_output.pdf}
\end{center}
\caption{The input is a pair of images from the left and right camera.
Note that objects closer to the camera have larger disparities than objects
farther away. The output is a dense disparity map shown on the right, with
warmer colors representing larger values of disparity (and smaller values of
depth).}

\label{fig:input_output}
\end{figure*}

When an object is photographed, information about its 3D structure, as well as
its position, is lost. \emph{Stereo vision} is the process of reconstructing
the 3D model of the scene from two or more images by finding matching pixels in
the images and converting their 2D positions into 3D depths. A stereo method
attempts to invert the process of image formation---projecting points from the
image plane back into the scene---to retrieve the original 3D structure of
objects captured on film. Research on stereo methods dates back to 1970 and
stereo continues to be an active area of research with several new algorithms
published each year. 

Learning based approaches are the main topic of this thesis. Two key
factors---the introduction of large stereo data sets and the exponential growth
of computing power---have enabled learning methods to overtake traditional
stereo approaches in terms of accuracy. We focus on two-frame stereo methods
and assume that the cameras differ in horizontal location, but are otherwise
identical. While this assumption almost never holds for physical devices, it
can be achieved by a post-processing step known as image rectification.
We consider only dense stereo methods, which produce a depth estimate at each
pixel in an image.

The left and right images differ mostly in the horizontal location of objects;
other differences are caused by reflections, occlusions, and perspective
distortions. The horizontal displacement, or disparity, plays a crucial role in
determining the 3D position of points in the scene, as objects closer to the
image plane are displaced more than objects farther away. In fact, there is a
direct relationship between disparity and depth, as we saw in
Equation~\ref{eqn:disparity}, and the problem of reconstructing a scene from
two images reduces to the problem of determining disparities.

To estimate the disparity of a point in the left image a window-based stereo
method crops a small patch around that point and searches for the most
similar patch in the right image. The search for corresponding points needs to
be carried out only in one dimension---along the epipolar line. There are
several ways to define a similarity measure on image patches and the quality of
the disparity map depends strongly on the similarity measure used. The main
contribution of this thesis is a method that attempts to learn a similarity
measure on image patches.

The 3D structure of a scene can, alternatively, be obtained using active
sensors. A LIDAR sensor illuminates the target scene with laser light and
calculates the distance by measuring the time required for the signal to
return. Structured light is the process of projecting know patterns onto the
scene and determining the disparity from multiple images. The Microsoft Kinect
sensor projects an infrared speckle pattern, invisible to the human eye, onto
the scene to determine its 3D structure using only a single camera.

Stereo methods have some advantages over active sensor: unlike LIDAR, stereo
methods produce a dense disparity map; unlike the Kinect sensor and the
structured light method, stereo methods can be used everywhere, indoor and
outdoor; using a stereo method is usually less expensive and systems that
require depth information usually have cameras already installed; the cameras
themselves are typically smaller in size than some active sensors; and, since
only cameras are used, multiple stereo systems do not interfere with each
other. On the other hand, there are some advantages of using active sensors:
they are typically more accurate than stereo methods and can be used in
challenging conditions such as low light.

The output of a stereo algorithm is a dense disparity map, which can be used in
many applications such as obstacle detection; object recognition and
localization; autonomous driving; navigation; 3D mapping; image
post-processing, for example, refocusing and background subtraction;
intermediate view generation; and cartography.

\section{Scientific Contributions}
\input{chapter_contributions}
