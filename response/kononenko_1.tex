\documentclass[12pt]{article}
\usepackage{booktabs}
\usepackage{geometry}
\usepackage[parfill]{parskip}
\usepackage{url}
\usepackage{graphicx}

\begin{document}

\section{Important comments}

\textbf{1)  The whole search for appropriate architecture and appropriate
parameter setting seems to be ad-hoc, data driven (using cross validation) with
search in a huge space of different candidate models (as described on page 65)
where probably exhaustive search (brute force) was applied, and a huge space of
possible parameter values – although obviously not all possible combination of
parameter values were tried. As a result of reading your thesis I CANNOT NOT to
agree with the opinion of prof. Vladimir Vapnik, that deep neural networks are
ad-hoc brute-force methodology, where for a new problem you  have to try a huge
number of different architectures/models and have to tune the parameters
extensively and if you are lucky, you will get excellent performance, without
knowing why does it work (as opposed to smart scientific approach where you
understand the problem and select only one or a few potentially appropriate
models).}

I can see how my dissertation can give the impression that in order to get good
results with neural networks one has to search over a large space of architecture
choices, possible loss functions, and hyperparameter values; and a large
portion of my research time was, in fact, spent exploring this space. However,
the following facts must not be overlooked:

\begin{itemize}
\item \emph{The neural network for stereo is quite robust with respect to the
values of hyperparameters.}

Consider the accurate architecture on the KITTI 2012 dataset in table 4.11 on
page 93. The validation error ranges from 2.61\,\% to 2.76\,\% for all tested values of
the following hyperparameters: \texttt{num\_fc\_units},
\texttt{dataset\_neg\_high}, \texttt{dataset\_neg\_low} and
\texttt{num\_fc\_units}. The difference between the worst and best performing
networks in this case is marginal and only worth optimizing in a ``competition
setting''. 

The parameters that can produce networks with high validation errors are
\texttt{num\_conv\_layers}, \texttt{num\_conv\_feature\_maps}, and
\texttt{num\_fc\_layers}. We observed that the only networks with a high
validation error were networks with less than three convolutional layers, less
than three fully-connected layers, or networks with a small number of feature
maps. 

In our experiments with stereo, we did not find that we would {\em have to try
a huge number of different architectures/models and have to tune the parameters
extensively} to get excellent performance. We observed, instead, that most
hyperparameter settings achieve excellent performance.

\item \emph{The search over the space of hyperparameters is not unique to
neural networks.} 

The hyperparameter search problem would still be present if we used other
supervised machine learning algorithms, for example logistic regression or
support vector machines, instead of neural networks. In logistic regression we
would have to choose the type and strength of regularization; in support vector
machines the hyperparameters are the type and shape of the kernel and the
strength of regularization. In the paper \emph{``A Practical Guide to Support
Vector Classification''}~\cite{hsu2003practical}, the recommended approach to
fitting these two hyperparameters is brute-force grid-search.


\item \emph{Most hyperparameters control the behaviour of the stereo method and
not the neural network.} 

Consider all 21 hyperparameters listed in Table 4.5 on page 82. The first six
hyperparameters control the neural network, the next three are used for the
dataset construction step, and the last twelve hyperparameters are used by the
stereo method. From an engineering point of view, the stereo method was
actually much harder to get working than the neural network training. 

\end{itemize}

\textbf{2)  The major contribution of the thesis is a new method (i.e. two
different methods) for evaluating the matching costs. The developed methods
should be generally applicable, however, for each training data a separate
(hyper)parameter tuning is required, which is performed manually – as stated on
page 83 (the end of Section 4.3). - Give exact set of all possible values for
each hyperparameter (not intervals of real numbers, as there is in
(uncountable) infinity in any such interval).}

The intervals that we explored are given in Table 4.11. The hyperparameters of
the matching cost neural network that have a noticeable effect on the
validation error are the following:

\begin{itemize}
\item The number of convolutional layers, \texttt{num\_conv\_layers}: $\{1,2,3,4,5,6\}$
\item The number of feature maps, \texttt{num\_conv\_feature\_maps}: 
$\{16, 32, 48, 64, 80, 96, 112, 128\}$
\item The number of fully-connected layers, \texttt{num\_fc\_layers}: $\{1,2,3,4,5\}$
\end{itemize}

As I have argued in the response to the previous question, the other
hyperparameters do not have a strong effect on the validation error and do not
need to be refit.

\textbf{Give the size of the Cartesian product of all sets of values – this
is the space, which you should search manually? With intuition? By guessing?
So, if I am to use your approach on a new dataset, I will use intuition? }

The size of the Cartesian product of the three sets of hyperparameter values is
$6 \cdot 8 \cdot 5 = 240$. While a full grid search is feasible, it can be
avoided by observing that larger networks, either in the number of layers or
the number of feature maps, usually achieve more accurate depth maps. 

Based on experience from training on the KITTI and Middlebury dataset (Table
4.11) my advice is to use at least 3 convolutional layers with at least 64
convolutional feature maps and at least 3 fully-connected layers.

\textbf{As you say at the beginning of Section 4.10, it is a daunting task.
So, I request that you describe in detail the algorithm for parameter tuning! }

I did not use an algorithm for hyperparameter tuning. Instead, I performed a
manual search of the hyperparameter space, guided by intuition.
As an alternative, several algorithms for hyperparameter optimization
were published recently \cite{bergstra2011algorithms, bergstra2012random, yamins2013making,
snoek2012practical} and can be used instead.

The dissertation was modified to address the first and second comments in the
following ways:

\begin{itemize}

\item{Added a paragraph to the end of Section ``Hyperparameters'':}
\begin{itemize}\item[]
The convolutional neural network for stereo is quite robust with respect to the
values of hyperparameters. We observed that most hyperparameter settings
achieve excellent performance. The networks performance degrades only when the
number of layers or the number of feature maps is small. Based on our
experience from training on the KITTI and Middlebury data sets, our advice is
to use at least 3 convolutional layers with at least 64 convolutional
feature maps and at least 3 fully-connected layers.
\end{itemize}

\item{Added a paragraph to the end of Section ``Details of Learning'':}

\begin{itemize}\item[]
We did not use an algorithm for hyperparameter tuning. Instead, we performed a
manual search of the hyperparameter space, guided by intuition. As an
alternative, several algorithms for hyperparameter optimization were published
recently \cite{bergstra2011algorithms, bergstra2012random, yamins2013making,
snoek2012practical} and can be used instead. The hyperparameters we selected
are shown in Table~\ref{tbl:hyperparameters_default}.
\end{itemize}

\end{itemize}

\textbf{3) When analyzing the performance of a new method with existing ones,
where the methods depend on the training set, one needs to perform a
significance test in order to become sure whether the differences are
significant or not (i.e. for different training sets the ranking of different
approaches may vary, and also the same is true for different testing sets). Of
course, a competition uses a different scenario, but a dissertation is not a
competition and in it a detailed analysis of performances is necessary.
Therefore, I request that you use available data to measure the performances of
your and existing approaches and test the significance of differences (your
co-supervisor being an expert in this...)  Currently I cannot agree with your
statement in conclusions on page 97 (line -2)}

I agree; a statistical analysis is important and should be part of the
dissertation. Thank you for pointing it out. I have added a new section titled
``Statistical Significance'' where I perform the statistical analysis suggested
by Dem\v{s}ar~\cite{demvsar2006statistical}.

\section{Required clarifications in the text}

\textbf{1) P 62, first prgrph:  You use during training pairs of examples for
fast architecture. Explain, how this minimization is performed in detail:
obviously this is not in the mood of neural networks which see one example at a
time and for training the local information suffices. For your minimization you
need the computation of two outputs, one for each training example and only
then you can calculate the loss and use its gradient?? to correct the weights??}

You are correct in your interpretation. I compute the outputs of the positive
and negative examples, then compute the gradients and adjust the weights. I have
added the following paragraph to make this clear:

\begin{itemize}\item[]
Since the hinge loss function depends on both the score of the positive as well 
as the score of the negative example, we have to ensure that both examples
appear in the same mini-batch during training. If the size of the mini-batch is
$n$, this is achieved by selecting $n/2$ random training images and positions;
one positive and one negative example is extracted from each position as
described in Section~\ref{sec:dataset}. Once the outputs of all examples
of the mini-batch have been computed, the gradients are obtained by the
backpropagation algorithm and the weights are updated accordingly.
\end{itemize}

\textbf{The setting of margin to 0.2 seems ad-hoc.}

Since the last layer of the fast architecture computes the cosine similarity,
the output of the network is limited to the interval [-1, 1] and the network
can't arbitrarily increase the margin by scaling the feature vectors. This
could be the reason why the validation error is different for a margin of 1 and
a margin of 0.2.

The margin was treated as a hyperparameter. The value of 0.2 was chosen by
performing a search over a range of possible margin values.
Table~\ref{tbl:margin} shows how the margin affects the validation error on the
KITTI 2012 data set.

\begin{table}[h]
\begin{center}
\begin{tabular}{cc}\toprule
Margin & Validation error \\\midrule
0.00 & 1.000 \\
0.05 & 0.039 \\
0.10 & 0.032 \\
0.15 & 0.031 \\
0.20 & 0.030 \\
0.25 & 0.030 \\
0.30 & 0.030 \\
0.40 & 0.031 \\
0.60 & 0.032 \\
0.80 & 0.033 \\
1.00 & 0.035 \\
\bottomrule
\end{tabular}
\end{center}
\caption{The table shows how the margin parameter affects validation error on
KITTI 2012 using the fast architecture.}

\label{tbl:margin}
\end{table}

If the margin is zero, the training collapses because the network can minimize the
loss function by mapping everything to zero. This result was expected. The
validation error is quite stable for margins greater than 0.05. The best value
for the margin is from 0.2 to 0.3 on this data set with this architecture.

The following sentence was added to the dissertation:

\begin{itemize}\item[]
The margin was treated as a hyperparameter and was set to 0.2 after performing
a grid search over the range from 0 to 1.
\end{itemize}


\textbf{For Accurate architecture you use one training example at a time. The
selection of two different loss function was again ad-hoc.}

The choice of loss function was based on empirical observations. We hoped one
loss function would work for both the fast architecture and the accurate
architecture, but experiments showed that the binary cross-entropy loss works
well for the accurate architecture and the hinge loss works well for the fast
architecture.

We ran the following experiments on the KITTI 2012 data set to validate our
claim:

\begin{enumerate}
\item Using the hinge loss with the accurate architecture (instead of the
binary cross-entropy loss as in the paper) increased the validation error from
2.61\,\% to 2.73\,\%.

\item Using the binary cross-entropy loss with the fast architecture (instead
of the hinge loss as in the paper) increased the validation error from 3.02\,\%
to 4.68\,\%. Since the output of the fast architecture is constrained between
$-1$ and $1$ we had to make some changes to the network. Concretely, we removed the
normalization layer and added the sigmoid nonlinearity as the final step of the
network.

\end{enumerate}

We also observed that other researches used similar loss function for siamese
architectures~\cite{weston2011wsabie, hadsell2006dimensionality} and typically
used a cross-entropy loss when the network resembled our accurate architecture.

\textbf{2) P 64, Designing the architecture, raw image intensity values or
feature vectors: Did you try both??}

To compare the effects of using raw image intensity values instead of the
extracted feature vectors I removed all the convolutional layers and recomputed
the validation error. The new architecture crops 9 $\times$ 9 image patches,
flattens them into a vectors of length 81 and concatenates the vectors from the
left and right image patch to produce a vector of length 162. The concatenated
vector is forward propagated through the fully-connected layers as usual. The
validation error of the new architecture on the KITTI 2012 data set is
2.87\,\%, which is larger than 2.61\,\% achieved when the similarity is
computed on features extracted from image patches.

If the goal is to make the network simpler or faster, a better place to remove
layers is after the feature vectors have been concatenated, as those layers
have the largest effect on the runtime. This is exactly what we did to derive
the fast architecture.

\textbf{3) On page 64 line -2: here the validation error is mentioned for the
first time. Exactly define it, before using it! What kind of cross validation
did you use? When presenting the results of cross validation a standard
deviation should also be provided.}

All validation errors were obtained using a single 80/20 split of the data
set. We decided not to use k-fold cross validation or repeated random
sub-sampling because of the following two reasons:

\begin{itemize}
\item The number of examples in the validation set is 5.1 million on KITTI
2012, 3.5 million on KITTI 2015, and 11.9 million on the Middlebury dataset.
Given the sizes of the validation sets, a single split should be enough to
estimate the error rate.

\item Since the average training time for a single split is approximately one
day on a high-end GPU, using anything more than a single split would be rather
challenging (consider that Table 4.11 alone contains roughly 200 validation
errors).

\end{itemize}

I have added the following sentence, which clarifies that we are using a single
split of the data set:

\begin{itemize}\item[]
the validation error rates were computing using hold-out cross validation, with
80\,\% of the examples used for training and 20\,\% for testing.
\end{itemize}

\textbf{4) P 66, middle: Here you discuss that the output can be computed for
all pixels in a single forward pass for full resolution images rather than
small image patches. Clarify, how do you deal with different sizes of the input
(the number of pixels). Later on it seems, that when using small image patches,
you concatenate them to obtain the same image size – is this correct? (P80,
Section 4.3 – I assume that concatenating 128 image patches gives the same size
as one full size image, is that correct??)}

I do not believe your interpretation is correct, that is, concatenating 128
image patches does not give the same size as one full size image. The key
insight is to understand that a convolutional layer can be used on inputs with
arbitrary spatial dimensions (number of pixels). The convolutional layer will
preserve the resolution of the input---minus the border effects---by
replicating the feature detectors. I have added the following paragraph to make
this easier to grasp:

\begin{itemize}\item[]
To make this step more concrete and easier to understand, let us calculate the 
sizes of the input and output tensors of the first convolutional layer. Assume
that the batch size is 128, the number of feature maps is 112, the size of the
kernel is $3 \times 3$, the patch size is $9 \times 9$, and the size of the
test image is $800 \times 600$. During training the input to the first
convolutional layer is $128 \times 1 \times 9 \times 9$ and the output is $128
\times 112 \times 7 \times 7$. The spatial resolution decreases from $9 \times
9$ to $7 \times 7$ because of the boundary effects of convolution. Since the
output of the convolutional layer is computed by replicating the 112 feature
detectors at each position in the image, we can apply the same convolutional
layer to inputs of different spatial dimensions. At test time the input to the first
convolutional layer is $2 \times 1 \times 800 \times 600$ and the output is $2
\times 112 \times 800 \times 600$. The first dimension is 2, because we forward
propagate the left and right image. Note that in order to preserve the spatial
resolution of $800 \times 600$ we add zero padding at test time.
\end{itemize}


\textbf{5) P 75: Clarify, how many times did you use the testing opportunity on
KITTI 2012 dataset (I assume that on KITTI 2015 you did it only once, is this
correct?)}

Unfortunately, I did not keep track of the number of submissions made to the
KITTI evaluation server, nor was I able to obtain this information on their
website. My best guess would be between five and ten submission on the KITTI
2012 data set. The test set was used once for each architecture on the
KITTI 2015 and Middlebury data sets.

\textbf{6) Figure 4.3: Give also running times for this example for different
approaches.}

Since Figure 4.3 on page 81 contains no mention of running times, I am not
sure what this questions is referring to. Could you please check that you were indeed
referring to Figure 4.3 on page 81?

\textbf{7) P. 85: One training example is composed of 64 positives and 64
negative subexamples. Explain in detail, how you can use at the same time one
training example that is composed of positive and negative subparts??}

I could not find the sentence \emph{``One training example is
composed of 64 positives and 64 negative subexamples''} on page 85, nor
anywhere else in the dissertation. Could you please provide the page and
line number of the sentence you are referring to.

I probably meant to say the following: \emph{``a batch of 128 examples is
composed of 64 examples belonging to the positive class and 64 examples
belonging to the negative class.''}. The positive and negative examples are
derived from the same position on the image as described in detail in Section
3.1.1.

\textbf{8) P 85: number of epochs, initial learning rate and its decrease
schedule were optimized using cross-validation. DESCRIBE IN DETAIL THIS PROCESS
OF OPTIMIZATION – WAS IT AGAIN MANUAL OR IT IS AUTOMATED??}

The process of tuning the training procedure was manual. The learning rate was
selected by experimenting with several candidate values and choosing the highest value
for which the training was still stable. We measured the validation error after
each epoch and found that it stopped decreasing after around 10 epochs.
Dividing the learning rate by 10 when the validation error plateaus is a common
learning rate decrease schedule used in the
literature~\cite{krizhevsky2012imagenet, simonyan2014very, he2015deep}. After
an additional 4 epochs the validation error stopped decreasing again. We tried
dividing the learning rate by 10 for the second time, but since the validation
error did not decrease, we stopped the training after 14 epochs.

I modified a sentence in the dissertation, to make it clear that the process of
tuning the training procedure was manual.

\begin{itemize}\item[]
The number of epochs, the initial learning rate, and the learning rate decrease
schedule where treated as hyperparameters and were optimized manually.
\end{itemize}

\textbf{9) P 85. Last line: Where does Tiny data set come from? What does it
mean that it is fictitious?}

The Tiny dataset is not a real stereo data set, but a fake data set used only to
measure the running time performance. I have rewritten the paragraph in
question, which now reads:

\begin{itemize}\item[]
Table~\ref{tbl:runtime} contains the runtime measurements across a range of
hyperparameter settings for three data sets: KITTI, Middlebury half resolution,
and a new, fictitious data set, called Tiny. The Tiny data set is not a real
stereo dataset; it contains only one image pair comprising two black images of
size 320 \(\times\) 240. We run our method on this data set only to measure its
runtime on the kind of images typically used for autonomous driving or
robotics.
\end{itemize}

\bibliographystyle{plain}
\bibliography{../thesis}
\end{document}
